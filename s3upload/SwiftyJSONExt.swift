//
//  SwiftyJSONExt.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/12/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import SwiftyJSON

extension JSON {
    func parseId()->String? {
        if let id = self.number {
            return String(id)
        } else {
            return self.string
        }
    }
    
    func parseDateRFC3999()->NSDate? {
        if let dateStr = self.string {
            return rfc3999DateFormatter.dateFromString(dateStr)
        }
        return nil
    }
    
    func parseDateUnixTime()->NSDate? {
        if let dateStr = self.string {
            if let dateDouble = Double(dateStr) {
                return NSDate(timeIntervalSince1970: dateDouble)
            }
        } else if let dateNumber = self.number {
            return NSDate(timeIntervalSince1970: dateNumber.doubleValue)
        }
        return nil
    }
    
    func parseBool()->Bool {
        if let dataStr = self.string {
            if dataStr == "y" {
                return true
            }
        }
        return false
    }
    
    func isHttpOk()->Bool {
        if let statusCode = self["_statusCode"].int {
            if statusCode == 200 || statusCode == 201 {
                return true
            }
        }
        return false
    }
    
    func nonEmptyString()->String? {
        if (self.string ?? "").trim().isEmpty {
            return nil
        }
        return self.string
    }
    
    func parseDate(dateFormatter:NSDateFormatter)->NSDate? {
        if let string = self.string {
            return dateFormatter.dateFromString(string)
        }
        return nil
    }
}


// get api message
extension NSData {
    
    func getApiMessage()->String? {
        let json = JSON(data:self)
        if let msg = json["message"].string {
            return msg
        }
        return nil
    }
}