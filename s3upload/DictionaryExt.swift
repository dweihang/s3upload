//
//  DictionaryExt.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/2/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func merge(other:Dictionary?) {
        if other != nil {
            for (key,value) in other! {
                self.updateValue(value, forKey:key)
            }
        }
    }
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
            let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        
        return parameterArray.joinWithSeparator("&")
    }
}