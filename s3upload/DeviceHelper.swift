//
//  DeviceHelper.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import UIKit

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0 && UIScreen.mainScreen().nativeScale <= 2
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0 && UIScreen.mainScreen().nativeScale == UIScreen.mainScreen().scale
    static let IS_ZOOMED_IPHONE_6   = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0 && UIScreen.mainScreen().nativeScale > 2
    static let IS_ZOOMED_IPHONE_6P  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0 && UIScreen.mainScreen().nativeScale > UIScreen.mainScreen().scale
    static let IS_IPAD_LT1024              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH < 1024
    static let IS_IPAD_EQ1024       = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024
    static let IS_IPAD_GT1024          = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH > 1024
}

struct iOSVersion {
    static let SYS_VERSION_FLOAT = (UIDevice.currentDevice().systemVersion as NSString).floatValue
    static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
    static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
}

// iphone 6 based nomral display layout factor
struct ScaleFactor {
    static let HORIZONTAL:CGFloat = {
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_ZOOMED_IPHONE_6 || DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPAD_LT1024 || DeviceType.IS_IPAD_EQ1024 {
            return CGFloat(320)/CGFloat(375)
        } else if DeviceType.IS_IPHONE_6P && !DeviceType.IS_ZOOMED_IPHONE_6P {
            return CGFloat(414)/CGFloat(375)
        }
        return 1
    }()
    
    static let VERTICAL:CGFloat = {
        if DeviceType.IS_IPHONE_5 || DeviceType.IS_ZOOMED_IPHONE_6 {
            return CGFloat(568)/CGFloat(667)
        } else if DeviceType.IS_IPHONE_6P && !DeviceType.IS_ZOOMED_IPHONE_6P {
            return CGFloat(736)/CGFloat(667)
        } else if DeviceType.IS_IPHONE_4_OR_LESS || DeviceType.IS_IPAD_LT1024 || DeviceType.IS_IPAD_EQ1024 {
            return CGFloat(480)/CGFloat(667)
        }
        return 1
    }()
}


struct DeviceModel {
    static private(set) var MODEL:String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }()
}

//func EMSystemSoundFinishedPlayingCallback(soundId: SystemSoundID, userData: UnsafeMutablePointer<Void>) {
//    AudioServicesDisposeSystemSoundID(soundId);
//}
//
//class DeviceManager {
//    // static let sharedInstance = DeviceManager()
//    private init() {}
//    
//    /**
//     * 播放提示语音
//     * 已经经过测试了，一切OK
//     */
//    static func playRing() -> SystemSoundID {
//        let audioPathStr = NSBundle.mainBundle().pathForResource("ring", ofType: "caf")
//        if let audioPathStr = audioPathStr {
//            let audioPathUrl = NSURL(fileURLWithPath: audioPathStr)
//            var soundId: SystemSoundID = UINT32_MAX
//            AudioServicesCreateSystemSoundID(audioPathUrl, &soundId)
//            let osstate: OSStatus = AudioServicesAddSystemSoundCompletion(soundId, nil, nil, EMSystemSoundFinishedPlayingCallback, nil)
//            AudioServicesPlaySystemSound(soundId)
//            print("\(osstate)")
//            return soundId
//        } else {
//            return UINT32_MAX
//        }
//    }
//    
//    // 震动
//    static func playVibration() -> Void {
//        AudioServicesAddSystemSoundCompletion(
//            kSystemSoundID_Vibrate,
//            nil, // uses the main run loop
//            nil, // uses kCFRunLoopDefaultMode
//            EMSystemSoundFinishedPlayingCallback, // the name of our custom callback function
//            nil // for user data, but we don't need to do that in this case, so we just pass NULL
//        );
//        
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
//    }
//}



