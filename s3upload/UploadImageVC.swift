//
//  UploadImageVC.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import UIKit
import SnapKit
import Photos
import Bolts

class UploadImageVC: UIViewController {

    var imgPickerDataSource:DHImagePickerDataSource!
    
    var photosCollectionView:UICollectionView!
    var photosImages:[(phAsset:PHAsset?, collectionImg:UIImage)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.imgPickerDataSource = DHImagePickerDataSource()
        self.imgPickerDataSource.maxCount = 6
        self.view.backgroundColor = UIColor.whiteColor()
        
        updateNavigationBar()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Upload", style: UIBarButtonItemStyle.Plain, handler: { [unowned self] (sender) in
            if self.imgPickerDataSource.assets.count > 0 {
                self.finishPickingPhoto()
            }
        })
        
        
        let itemSize = CGSizeMake(UIScreen.mainScreen().bounds.width, 317)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = itemSize
        flowLayout.minimumLineSpacing = 0
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.scrollDirection = .Horizontal
        flowLayout.sectionInset = UIEdgeInsetsZero
        
        self.photosCollectionView = UICollectionView(frame: CGRectMake(0, 0, self.view.width, 317), collectionViewLayout: flowLayout)
        self.photosCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "ImageCell")
        self.view.addSubview(photosCollectionView)
        self.photosCollectionView.delegate = self
        self.photosCollectionView.dataSource = self
        self.photosCollectionView.pagingEnabled = true
        
        let addPhotoBtn = UIButton()
        addPhotoBtn.setTitle("add photo".localized, forState: .Normal)
        addPhotoBtn.layer.cornerRadius = 3
        addPhotoBtn.layer.masksToBounds = true
        addPhotoBtn.setBackgroundColor(StyleHelper.getAppThemeColor(), forUIControlState: .Normal)
        addPhotoBtn.setBackgroundColor(StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.9), forUIControlState: .Highlighted)
        self.view.addSubview(addPhotoBtn)
        let tabBarHeight = tabBarController?.tabBar.bounds.size.height ?? 0
        addPhotoBtn.snp_makeConstraints { (make) in
            make.bottom.equalTo(self.view.snp_bottom).offset(-20 - tabBarHeight)
            make.width.equalTo(self.view.width - 40)
            make.height.equalTo(40)
            make.centerX.equalTo(self.view.snp_centerX)
        }
        
        addPhotoBtn.addTarget(.TouchUpInside) { 
            [unowned self] in
            self.showImagePickerSheet()
        }
    }
    
    func showImagePickerSheet() {
        let actionSheet = CustomActionSheet(title: nil, cancelButtonTitle: "cancel".localized, otherButtonTitles: ["take photo".localized, "pick from gallery".localized])
        actionSheet.delegate = self
        actionSheet.show()
    }

    func updateNavigationBar() {
        if self.imgPickerDataSource.assets.count < 2 {
            self.navigationItem.title = String(format: "%d Photo", self.imgPickerDataSource.assets.count)
        } else {
            self.navigationItem.title = String(format: "%d Photos", self.imgPickerDataSource.assets.count)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension UploadImageVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return max(self.imgPickerDataSource.assets.count, 1)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCell", forIndexPath: indexPath)
        
        var imgView:UIImageView? = cell.viewWithTag(7734) as? UIImageView
        if imgView == nil {
            imgView = UIImageView()
            imgView!.contentMode = UIViewContentMode.ScaleAspectFill
            imgView!.tag = 7734
            imgView?.clipsToBounds = true
            cell.contentView.addSubview(imgView!)
            
            imgView?.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 317)
            
//            imgView?.snp_makeConstraints(closure: { (make) in
//                make.edges.equalTo(cell.contentView)
//            })
        }
        
        var cameraView:UIImageView? = cell.viewWithTag(6590) as? UIImageView
        if cameraView == nil {
            cameraView = UIImageView()
            cameraView?.tag = 6590
            let cameraImg = UIImage(named: "camera.png")!
            cameraView?.image = cameraImg
            cameraView?.frame = CGRectMake(0, 0, cameraImg.size.width, cameraImg.size.height)
            cell.contentView.addSubview(cameraView!)
            cameraView?.snp_makeConstraints(closure: { (make) in
                make.centerX.equalTo(cell.contentView.snp_centerX)
                make.centerY.equalTo(cell.contentView.snp_centerY)
            })
        }
        
        var addImageLabel:UILabel? = cell.viewWithTag(4358) as? UILabel
        if addImageLabel == nil {
            
            addImageLabel = UILabel()
            addImageLabel?.tag = 4358
            addImageLabel?.text = "add photo".localized
            addImageLabel?.font = UIFont.systemFontOfSize(17)
            addImageLabel?.textColor = UIColor.whiteColor()
            cell.contentView.addSubview(addImageLabel!)
            addImageLabel?.snp_makeConstraints(closure: { (make) in
                make.centerX.equalTo(cell.contentView.snp_centerX)
                make.top.equalTo(cameraView!.snp_bottom).offset(5)
            })
            addImageLabel?.sizeToFit()
        }
        
        if self.imgPickerDataSource.assets.count == 0 {
            imgView?.image = nil
            imgView?.backgroundColor = UIColor(red: 207/255, green: 207/255, blue: 207/255, alpha: 1)
            cameraView?.hidden = false
            addImageLabel?.hidden = false
        } else  {
            imgView?.image = photosImages[indexPath.row].collectionImg
            cameraView?.hidden = true
            addImageLabel?.hidden = true
        }

        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.imgPickerDataSource.assets.count == 0 {
            showImagePickerSheet()
        } else {
            let vc = DHPreviewController()
            vc.doneClosure = { [unowned self] in
                if self.imgPickerDataSource.assets.count != self.photosImages.count {
                    var vals:[NSObject:Bool] = [:]
                    for asset in self.imgPickerDataSource.assets {
                        if let phAsset = asset.photoAsset {
                            vals[phAsset] = true
                        }
                        if let photo = asset.image {
                            vals[photo] = true
                        }
                    }
                    
                    self.photosImages =  self.photosImages.filter({ (item: (phAsset: PHAsset?, collectionImg: UIImage)) -> Bool in
                        if vals[item.collectionImg] == true {
                            return true
                        }
                        if let asset = item.phAsset {
                            if vals[asset] == true {
                                return true
                            }
                        }
                        return false
                    })
                    self.photosCollectionView.reloadData()
                }
                self.updateNavigationBar()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            
            vc.pickerDataSource = self.imgPickerDataSource
            vc.currentIndex = indexPath.row
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
}

extension UploadImageVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgPickerDataSource.insertImageAtHead(image, isOriginalPhoto: true)
            self.imgPickerDataSource.commit()
            self.dismissViewControllerAnimated(true, completion: nil)
            self.photosImages.insert((nil,image), atIndex: 0)
            self.photosCollectionView.reloadData()
            self.updateNavigationBar()
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}

extension UploadImageVC: ActionSheetDelegate {
    
    func actionSheet(actionSheet: CustomActionSheet, buttonClickIndex: Int) {
        if buttonClickIndex == 0 { // 选择拍照
            actionSheet.animateOff({ (Void) in
                dispatch_async(dispatch_get_main_queue(), { [unowned self] in
                    let cameraMediaType = AVMediaTypeVideo
                    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(cameraMediaType)
                    
                    switch cameraAuthorizationStatus {
                        
                    case .Authorized:
                        self.showCameraPicker()
                        break
                    case .Restricted, .Denied:

                        break
                    case .NotDetermined:
                        AVCaptureDevice.requestAccessForMediaType(cameraMediaType) { [unowned self] granted in
                            if granted {
                                dispatch_async(dispatch_get_main_queue(), {
                                    [unowned self] in
                                    self.showCameraPicker()
                                    })
                            } else {
                                
                                dispatch_async(dispatch_get_main_queue(), {

                                })
                                
                            }
                        }
                    }
                    })
            })
            
            
        } else { // 选择相册
            actionSheet.animateOff()
            if hasGalleryPermission() {
                /**
                 * 跳转到相册
                 */
                let imagePickerVc = DHImagePickerController(dataSource: self.imgPickerDataSource)
                imagePickerVc.dhipcDelegate = self
                imagePickerVc.bottomBarColor = StyleHelper.getAppThemeColor()
                imagePickerVc.navigationBarColor = StyleHelper.getAppThemeSecondaryColor()
                self.presentViewController(imagePickerVc, animated: true, completion: nil)
            } else {
                let authorizationStatus: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
                switch authorizationStatus {
                case .Restricted, .Denied:
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    })
                    
                    break
                case .NotDetermined:
                    PHPhotoLibrary.requestAuthorization({ [unowned self] (status) in
                        
                        if status == .Authorized {
                            dispatch_async(dispatch_get_main_queue(), { [weak self] () -> Void in
                                if let storngSelf = self {
                                    let imagePickerVc = DHImagePickerController(dataSource: storngSelf.imgPickerDataSource)
                                    imagePickerVc.delegate = self
                                    imagePickerVc.navigationBarColor = StyleHelper.getAppThemeSecondaryColor()
                                    imagePickerVc.bottomBarColor = StyleHelper.getAppThemeColor()
                                    storngSelf.presentViewController(imagePickerVc, animated: true, completion: nil)
                                }
                                })
                        } else {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            })
                        }
                        })
                default:
                    break
                }
                
            }
        }
    }
    
    func cancelButtonClick(actionSheet: CustomActionSheet) {
        actionSheet.animateOff()
    }
    
    /**
     * 选择摄像头
     */
    func showCameraPicker() {
        if (UIImagePickerController.isSourceTypeAvailable(.Camera)) {
            if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
                let imagePicker = UIImagePickerController()
                //                    imagePicker.delegate = self
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func hasGalleryPermission() -> Bool {
        var hasGalleryPermission: Bool = false
        let authorizationStatus: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if authorizationStatus == .Authorized {
            hasGalleryPermission = true
        }
        return hasGalleryPermission
    }
}


extension UploadImageVC: DHImagePickerControllerDelegate {
    
    func finishPickingPhoto() {
        self.dismissViewControllerAnimated(true, completion: nil)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let hud = HUDHelper.showLoadingHudWithMessage("uploading".localized, parentView: self.navigationController?.view ?? self.view)
            ApiHelper.sharedInstance.S3batchUploadImagesWithDatasource(self.imgPickerDataSource).continueWithExecutor(BFExecutor.mainThreadExecutor(), withBlock: { [weak self] (task) -> AnyObject? in
                self?.imgPickerDataSource.clear()
                self?.photosImages.removeAll()
                self?.photosCollectionView.reloadData()
                self?.updateNavigationBar()
                hud.hide(true)
                if task.error != nil {
                    if let strongSelf = self {
                        HUDHelper.showErrorHudWithMessage("upload fail".localized, durationInSecond: 1, parentView: strongSelf.navigationController?.view ?? strongSelf.view, finishedBlock: nil)
                    }
                } else {
                    if let strongSelf = self {
                        HUDHelper.showSuccessHudWithMessage("upload successful".localized, durationInSecond: 1, parentView: strongSelf.navigationController?.view ?? strongSelf.view, finishedBlock: nil)
                    }
                }
                return nil
            })
        })
    }
    
    func didFinishPickPhoto(vc:DHImagePickerController) {
        let hud = HUDHelper.showLoadingHudWithMessage("processing images".localized, parentView: vc.view)
        var images:[(phAsset:PHAsset?, collectionImg:UIImage)] = []
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
            for i in 0 ..< self.imgPickerDataSource.assets.count {
                if let image = self.imgPickerDataSource.assets[i].image {
                    images.append((nil, image))
                } else if let phAsset = self.imgPickerDataSource.assets[i].photoAsset {
                    let opts = PHImageRequestOptions()
                    opts.networkAccessAllowed = true
                    opts.deliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat
                    opts.synchronous = true
                    var shouldContinue = false
                    for i in 0 ..< self.photosImages.count {
                        if phAsset == self.photosImages[i].phAsset {
                            images.append(self.photosImages[i])
                            shouldContinue = true
                            break
                        }
                    }
                    if shouldContinue {
                        continue
                    }
                    PHImageManager.defaultManager().requestImageForAsset(phAsset, targetSize: CGSizeMake(UIScreen.mainScreen().bounds.width, 317), contentMode: PHImageContentMode.AspectFill, options: opts, resultHandler: { (image, _) in
                        if let img = image {
                            images.append((phAsset, img))
                        }
                    })
                }
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                hud.hide(true)
                self.dismissViewControllerAnimated(true, completion: nil)
                self.updateNavigationBar()
                self.photosImages.removeAll()
                self.photosImages = images
                self.photosCollectionView.reloadData()
                
            })
        })
    }
    
    func didCancelPickPhoto(vc:DHImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

