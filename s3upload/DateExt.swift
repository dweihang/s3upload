//
//  DateExt.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/12/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation

var rfc3999DateFormatter:NSDateFormatter {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
    return formatter
}

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs === rhs || lhs.compare(rhs) == .OrderedSame
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending
}

extension NSDate: Comparable {}