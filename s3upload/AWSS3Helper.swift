//
//  AWSS3Helper.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import AWSS3
import AWSCore

typealias AWSUploadCompleteBlock = (key:String, error:NSError?) -> (Void)
typealias AWSProgressBlock = (progress:Float) -> (Void)
typealias AWSDownloadCompleteBlock = (fileUrl:NSURL, error:NSError?)


let S3_BUCKET = "cozystay-data"

class AWSS3Helper: NSObject {
    
    var s3:AWSS3
    
    static let sharedInstance = AWSS3Helper()
    
    override init() {
        let prodivder = AWSStaticCredentialsProvider.init(accessKey: "AKIAIC57RJOU5CLV455Q", secretKey: "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")
        let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: prodivder)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        s3 = AWSS3.defaultS3()
        super.init()
    }
    
    func uploadImageToS3(image:UIImage, completionBlock:AWSUploadCompleteBlock?, progressBlock:AWSProgressBlock?) {
        let req:AWSS3PutObjectRequest = AWSS3PutObjectRequest()
        let imageData = UIImageJPEGRepresentation(image, 1.0)
        let key = gennerateKeyWithExtension("jpg")
        req.bucket = S3_BUCKET
        req.key = key
        req.body = imageData
        req.contentType = "image/jpeg"
        req.contentLength = NSNumber(integer: imageData!.length)
        s3.putObject(req).continueWithBlock { (task) -> AnyObject? in
            if let _completeblock = completionBlock {
                dispatch_async(dispatch_get_main_queue(), { 
                    _completeblock(key: key, error: task.error)
                })
            }
            return nil
        }
    }
    
    private func gennerateKeyWithExtension(ext:String)-> String {
        return "mobile/upload/\(NSUUID().UUIDString).\(ext)"
    }
    
    
}



