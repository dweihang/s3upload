//
//  HUDHelper.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//
import Foundation
import MBProgressHUD

typealias HUDWillDisapearHandler = () -> Void

class HUDHelper:NSObject {
    
    private class func showSuccessHudWithMessage(message:String?, duration:dispatch_time_t, parentView:UIView, finishedBlock:HUDWillDisapearHandler?){
        let hud = MBProgressHUD.showHUDAddedTo(parentView, animated: true)
        hud.dimBackground = false
        hud.mode = MBProgressHUDMode.CustomView
        
        if let msg = message {
            //            let height = (msg as NSString).boundingRectWithSize(CGSizeMake(280, 22), options:[NSStringDrawingOptions.UsesLineFragmentOrigin, NSStringDrawingOptions.UsesFontLeading], attributes: [NSFontAttributeName:StyleHelper.getAppFontWithSize(16)], context: nil).height
            //            if height > 22 {
            hud.detailsLabelText = msg
            hud.detailsLabelFont = StyleHelper.getAppBoldFontWithSize(16)
        } else {
            //            hud.labelText = message ?? "成功！"
            hud.detailsLabelText = message ?? "成功!"
            hud.detailsLabelFont = StyleHelper.getAppBoldFontWithSize(16)
        }
        hud.customView = UIImageView(image: UIImage(named:"checkmark.png"))
        
        dispatch_after(duration, dispatch_get_main_queue(), { () -> Void in
            hud.hide(true)
            if finishedBlock != nil {
                finishedBlock!()
            }
        })
    }
    
    class func showSuccessHudWithMessage(message:String?, durationInSecond:Double, parentView:UIView, finishedBlock:HUDWillDisapearHandler?){
        let duration:Int64 = Int64(durationInSecond * Double(NSEC_PER_SEC))
        HUDHelper.showSuccessHudWithMessage(message, duration:  dispatch_time(DISPATCH_TIME_NOW, duration), parentView: parentView, finishedBlock: finishedBlock)
    }
    
    class func showTextHudWithText(text:String, durationInSecond:Double, parentView:UIView) {
        let hud = MBProgressHUD.showHUDAddedTo(parentView, animated: true)
        hud.dimBackground = true
        hud.mode = MBProgressHUDMode.CustomView
        let hudContainer = UIView(frame: CGRectMake(0, 0, 200 * ScaleFactor.HORIZONTAL, 183 * ScaleFactor.VERTICAL))
        hudContainer.backgroundColor = UIColor.clearColor()
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .Center
        let topPadding:CGFloat = 10
        let textPadding:CGFloat = 15
        label.textColor = UIColor.whiteColor()
        label.font = StyleHelper.getAppFontWithSize(17)
        label.text = text
        let size = label.sizeThatFits(CGSizeMake(200 * ScaleFactor.HORIZONTAL - 2 * textPadding, CGFloat.max))
        hudContainer.addSubview(label)
        label.snp_makeConstraints { (make) in
            make.left.equalTo(label.superview!.snp_left).offset(textPadding)
            make.right.equalTo(label.superview!.snp_right).offset(-textPadding)
            make.top.equalTo(label.superview!.snp_top).offset(topPadding)
            make.size.equalTo(size)
        }
        if 2 * topPadding + size.height > hudContainer.height {
            hudContainer.height =  2 * topPadding + size.height
        }
        hudContainer.height = 2 * textPadding + 2 * topPadding + size.height
        hud.customView = hudContainer
        hud.size = hudContainer.size
        let duration:Int64 = Int64(durationInSecond * Double(NSEC_PER_SEC))
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration), dispatch_get_main_queue(), { () -> Void in
            hud.hide(true)
        })
    }
    
    private class func showErrorHudWithMessage(message:String?, duration:dispatch_time_t, parentView:UIView, finishedBlock:HUDWillDisapearHandler?){
        let hud = MBProgressHUD.showHUDAddedTo(parentView, animated: true)
        hud.dimBackground = false
        hud.mode = MBProgressHUDMode.CustomView
        
        if let msg = message {
            hud.detailsLabelText = msg
            hud.detailsLabelFont = StyleHelper.getAppBoldFontWithSize(16)
        } else {
            hud.detailsLabelText = message ?? "发生未知错误"
            hud.detailsLabelFont = StyleHelper.getAppBoldFontWithSize(16)
        }
        
        hud.customView = UIImageView(image: UIImage(named:"crossmark.png"))
        
        dispatch_after(duration, dispatch_get_main_queue(), { () -> Void in
            hud.hide(true)
            if finishedBlock != nil {
                finishedBlock!()
            }
        })
    }
    
    class func showErrorHudWithMessage(message:String?, durationInSecond:Double, parentView:UIView, finishedBlock:HUDWillDisapearHandler?){
        let duration:Int64 = Int64(durationInSecond * Double(NSEC_PER_SEC))
        HUDHelper.showErrorHudWithMessage(message, duration:  dispatch_time(DISPATCH_TIME_NOW, duration), parentView: parentView, finishedBlock: finishedBlock)
    }
    
    
    class func showLoadingHudWithMessage(message:String?, parentView:UIView)-> MBProgressHUD{
        let hud = MBProgressHUD.showHUDAddedTo(parentView, animated: true)
        hud.dimBackground = true
        
        hud.labelText = message ?? "正在加载..."
        
        return hud
    }
    
}
