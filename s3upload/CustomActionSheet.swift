//
//  CustomActionSheet.swift
//  s3upload
//
//  Created by hy110831 on 7/22/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import UIKit

class ActionButton: UIButton {
    
    init(text:String, cancelable:Bool) {
        super.init(frame:CGRectZero)
        self.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 52)
        setTitle(text, forState: .Normal)
        self.backgroundColor = UIColor.whiteColor()
        self.titleLabel?.font = StyleHelper.getAppBoldFontWithSize(17)
        self.setTitleColor(StyleHelper.getActionSheetActionTextColor(), forState: .Normal)
        let separator = UIView()
        separator.backgroundColor = StyleHelper.getSeparatorColor()
        separator.frame = CGRectMake(0, self.frame.size.height - 0.5, self.frame.size.width, 0.5)
        self.addSubview(separator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class ActionTitleView: UIView {
    init(text:String) {
        super.init(frame:CGRectZero)
        self.frame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 52)
        let label = UILabel(frame: self.frame)
        self.backgroundColor = UIColor.whiteColor()
        label.text = text
        label.textColor = StyleHelper.getActionSheetTitleColor()
        label.font = StyleHelper.getAppFontWithSize(16)
        label.textAlignment = .Center
        self.addSubview(label)
        let separator = UIView()
        separator.backgroundColor = StyleHelper.getSeparatorColor()
        separator.frame = CGRectMake(0, self.frame.size.height - 0.5, self.frame.size.width, 0.5)
        self.addSubview(separator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


// MARK: ActionSheetDelegate
protocol ActionSheetDelegate:class {
    func actionSheet(actionSheet:CustomActionSheet, buttonClickIndex:Int)
    func cancelButtonClick(actionSheet:CustomActionSheet)
}

class CustomActionSheet: UIView {
    
    weak var delegate: ActionSheetDelegate?
    
    var dissmissOnclickBackground:Bool = true
    
    private var backgroundView:UIView!
    
    private var index:Int!
    
    private var yPosition:CGFloat!
    
    init(title:String?, cancelButtonTitle:String?, otherButtonTitles:[String]?) {
        // setup background frame
        super.init(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height))
        self.backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 0.65)
        
        backgroundView = UIView()
        backgroundView.backgroundColor = StyleHelper.getActionSheetBackgroundColor()
        backgroundView.userInteractionEnabled = true
        backgroundView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 380)
        
        yPosition = 0
        
        if title != nil {
            let titleLabel = ActionTitleView(text: title!)
            var frame = titleLabel.frame
            frame.origin.y = yPosition
            titleLabel.frame = frame
            backgroundView.addSubview(titleLabel)
            yPosition = yPosition + titleLabel.frame.size.height
        }
        
        if otherButtonTitles != nil {
            for i in 0 ..< otherButtonTitles!.count {
                let actionTitle = otherButtonTitles![i]
                let actionBtn = ActionButton(text: actionTitle, cancelable: false)
                var frame = actionBtn.frame
                frame.origin.y = yPosition
                actionBtn.frame = frame
                actionBtn.tag = i
                actionBtn.addSingleTapGestureRecognizerWithResponder({ [unowned self, unowned actionBtn](tap) in
                    self.delegate?.actionSheet(self, buttonClickIndex: actionBtn.tag)
                    })
                self.backgroundView.addSubview(actionBtn)
                yPosition = yPosition + actionBtn.frame.size.height
            }
        }
        
        if cancelButtonTitle != nil {
            yPosition = yPosition + 10
            let cancelBtn = ActionButton(text: cancelButtonTitle!, cancelable: true)
            var frame = cancelBtn.frame
            frame.origin.y = yPosition
            cancelBtn.frame = frame
            cancelBtn.addSingleTapGestureRecognizerWithResponder({ [unowned self](tap) in
                self.delegate?.cancelButtonClick(self)
                })
            backgroundView.addSubview(cancelBtn)
            yPosition = yPosition + cancelBtn.frame.size.height
        }
        
        var frame = backgroundView.frame
        frame.size.width = self.frame.size.width
        frame.size.height = yPosition
        backgroundView.frame = frame
        
        self.addSubview(backgroundView)
        self.animateOn()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func show() {
        UIApplication.sharedApplication().windows.first?.addSubview(self)
    }
    
    func animateOn() {
        UIView.animateWithDuration(0.23) { [unowned self] in
            self.backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 0.65)
            var frame = self.backgroundView.frame
            frame.origin.y -= self.yPosition
            self.backgroundView.frame = frame
        }
    }
    
    func animateOff() {
        self.delegate = nil
        UIView.animateWithDuration(0.23, animations: { [unowned self] in
            self.backgroundColor = UIColor.clearColor()
            var frame = self.backgroundView.frame
            frame.origin.y += self.yPosition
            self.backgroundView.frame = frame
        }) { [weak self] (_) in
            self?.removeFromSuperview()
        }
    }
    
    func animateOff(completionHandler:(Void)->(Void)) {
        self.delegate = nil
        UIView.animateWithDuration(0.23, animations: { [unowned self] in
            self.backgroundColor = UIColor.clearColor()
            var frame = self.backgroundView.frame
            frame.origin.y += self.yPosition
            self.backgroundView.frame = frame
        }) { [weak self] (_) in
            self?.removeFromSuperview()
            completionHandler()
        }
    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if self.dissmissOnclickBackground == true {
            let pointForTargetView = self.backgroundView.convertPoint(point, fromView:self)
            
            if (!CGRectContainsPoint(self.backgroundView.bounds, pointForTargetView)) {
                self.animateOff()
            }
        }
        return super.hitTest(point, withEvent: event)
    }
    
}