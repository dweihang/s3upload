//
//  AtomicBoolean.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/3/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation

class AtomicBoolean {
    
    private var val: Bool = false
    let lock = NSLock()
    
    init(val:Bool) {
        self.val = val
    }
    
    init() {
        self.val = false
    }
    
    func set(val:Bool) {
        synchronized(lock) { [unowned self] in
            self.val = val
        }
    }
    
    func get()->Bool {
        var val = false
        synchronized(lock) {
            val = self.val
        }
        return val
    }
}