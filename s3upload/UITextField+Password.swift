//
//  UITextField+Password.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/21/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import UIKit

class NotificationProxy: UIView {
    
    weak var objectProtocol: NSObjectProtocol!
    
    func addObserverForName(name: String?, object: AnyObject?, queue: NSOperationQueue?, usingBlock: (NSNotification!) -> ()) {
        
        // Register the specified object and notification with NSNotificationCenter
        self.objectProtocol = NSNotificationCenter.defaultCenter().addObserverForName(name, object: object, queue: queue, usingBlock: usingBlock)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        
        // Unregister the object from NSNotificationCenter
        NSNotificationCenter.defaultCenter().removeObserver(self.objectProtocol)
    }
}

let NOTIFS_TAG0 = 857
let NOTIFS_TAG1 = 986

extension UITextField {
    
    func setKeyboardPasswdHidden(hidden:Bool) {
        if hidden == false && self.viewWithTag(NOTIFS_TAG0) == nil {
            let notifisProxy = NotificationProxy()
            self.addSubview(notifisProxy)
            notifisProxy.tag = NOTIFS_TAG0
            notifisProxy.addObserverForName(
                
            UITextFieldTextDidChangeNotification, object: self, queue: nil) { [unowned self] (_) in
                self.secureTextEntry = false
            }
            
            let notifisProxy1 = NotificationProxy()
            self.addSubview(notifisProxy1)
            notifisProxy1.addObserverForName(UITextFieldTextDidBeginEditingNotification, object: self, queue: nil) { [unowned self] (_) in
                self.secureTextEntry = true
            }
        } else {
            if let view = self.viewWithTag(NOTIFS_TAG0) {
                view.removeFromSuperview()
            }
            if let view1 = self.viewWithTag(NOTIFS_TAG1) {
                view1.removeFromSuperview()
            }
            self.secureTextEntry = true
        }
    }
}