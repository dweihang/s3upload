//
//  DHImagePickerController.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/14/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import UIKit
import Photos

@objc protocol DHImagePickerControllerDelegate: class {
    
    func didFinishPickPhoto(controller:DHImagePickerController)
    
    func didCancelPickPhoto(controller:DHImagePickerController)
}

class DHImagePickerController: UINavigationController {
    
    var dataSource: DHImagePickerDataSource!
    var titleTextAttributes:[String:AnyObject]?
    
    weak var dhipcDelegate: DHImagePickerControllerDelegate?
    
    var doneClosure:((Void)->(Void))?
    var cancelClosure:((Void)->(Void))?
    
    var bottomBarColor:UIColor?
    var bottomBarHighLightedColor:UIColor?
    var navigationBarColor:UIColor? {
        didSet {
            if let color = navigationBarColor {
                self.navigationBar.barTintColor = color
            }
        }
    }

    convenience init(dataSource:DHImagePickerDataSource) {
        let albumPickerVC = DHAlbumPickerController()
        self.init(rootViewController: albumPickerVC)
        self.dataSource = dataSource
        setupNavigationBar()
        pushToPhotoPickerVCIfAlbumExists() // go to Library
    }
    
    
    deinit {
        print(#function, "\(self)")
    }
    
    func setupNavigationBar() {
        self.navigationBar.translucent = false
        self.navigationBar.barTintColor = StyleHelper.getAppThemeColor()
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barStyle = .Black
        if titleTextAttributes != nil {
            self.navigationBar.titleTextAttributes = titleTextAttributes
        } else {
            let attrs = [NSForegroundColorAttributeName:UIColor.whiteColor(), NSFontAttributeName: StyleHelper.getAppFontWithSize(18)]
            self.navigationBar.titleTextAttributes = attrs
        }
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        if self.viewControllers.count > 0 {
            let barBtnItem = UIBarButtonItem(image: UIImage(named: "dhpicker_nav_back.png"), style: .Plain, handler: { [unowned self](sender) in
                self.popViewControllerAnimated(true)
            })
            viewController.navigationItem.leftBarButtonItem = barBtnItem
        }
        super.pushViewController(viewController, animated: true)
    }
    
    func pushToPhotoPickerVCIfAlbumExists() {
        // 选择相册
        let userLibraryAlbum = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .SmartAlbumUserLibrary , options: nil).firstObject as? PHAssetCollection
        
        if let album = userLibraryAlbum {
            let options = PHFetchOptions()
            options.predicate = NSPredicate(format: "mediaType == %ld",PHAssetMediaType.Image.rawValue)
            let fetchResult = PHAsset.fetchAssetsInAssetCollection(album, options: options)
            if fetchResult.count > 0 {
                let photoPickerVC = DHPhotoPickerController(dataSource: dataSource, assetCollection: album, assetsFetchResult: fetchResult)
                self.pushViewController(photoPickerVC, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
