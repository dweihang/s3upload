//
//  DHImagePickerDataSource.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/14/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import Photos


class DHImagePickerDataSource:NSObject {
    
    // ture original photo,
    // false downsampled photo
    private(set) var assets:[DHImageAsset]
    private var assetsDict:[NSObject:DHImageAsset] = [:]
    
    var maxCount:Int
    
    var editAssets:[DHImageAsset]
    private var editAssetsDict:[NSObject:DHImageAsset] = [:]

    
    override init() {
        assets = []
        editAssets = []
        maxCount = 6
    }
    
    init(assets:[DHImageAsset], maxCount: Int) {
        self.assets = []
        self.editAssets = []
        for asset in assets {
            self.assets.append(asset)
            if let image = asset.image {
                self.assetsDict[image] = asset
            }
            self.editAssets.append(asset)
            if let photoAsset = asset.photoAsset {
                self.assetsDict[photoAsset] = asset
            }
        }
        self.maxCount = maxCount
    }
    
    deinit {
        assetsDict.removeAll()
        for asset in assets {
            asset.image = nil
            asset.photoAsset = nil
        }
        assets.removeAll()
        
        editAssetsDict.removeAll()
        for asset in editAssets {
            asset.image = nil
            asset.photoAsset = nil
        }
        editAssets.removeAll()
    }
    
    func clear() {
        self.editAssets.removeAll()
        self.commit()
    }
    
    func removeAssetByPHAsset(asset:PHAsset) {
        for (obj, _) in editAssetsDict {
            if let phAsset =  obj as? PHAsset {
                if phAsset.localIdentifier == asset.localIdentifier {
                    editAssetsDict[obj] = nil
                }
            }
        }
        var index2remove = -1
        for i in 0 ..< editAssets.count {
            if editAssets[i].photoAsset?.localIdentifier == asset.localIdentifier {
                index2remove = i
                break
            }
        }
        if index2remove >= 0 {
            editAssets.removeAtIndex(index2remove)
        }
    }
    
    func addAsset(asset:PHAsset, isOriginalPhoto:Bool)->DHImageAsset? {
        if editAssetsDict[asset] != nil {
            return nil
        }
        let imgAsset = DHImageAsset()
        imgAsset.photoAsset = asset
        imgAsset.isOriginalPhoto = isOriginalPhoto
        imgAsset.isSelected = true
        editAssets.append(imgAsset)
        editAssetsDict[asset] = imgAsset
        return imgAsset
    }
    
    func addImage(image:UIImage, isOriginalPhoto:Bool)->DHImageAsset? {
        if editAssetsDict[image] != nil {
            return nil
        }
        let imgAsset = DHImageAsset()
        imgAsset.image = image
        imgAsset.isOriginalPhoto = isOriginalPhoto
        imgAsset.isSelected = true
        editAssets.append(imgAsset)
        editAssetsDict[image] = imgAsset
        return imgAsset
    }
    
    func insertImageAtHead(image:UIImage, isOriginalPhoto:Bool)->DHImageAsset? {
        if editAssetsDict[image] != nil {
            return nil
        }
        let imgAsset = DHImageAsset()
        imgAsset.image = image
        imgAsset.isOriginalPhoto = isOriginalPhoto
        imgAsset.isSelected = true
        editAssets.insert(imgAsset, atIndex: 0)
        editAssetsDict[image] = imgAsset
        return imgAsset
    }
    
    func getEditAssetsSelectedCount()->Int {
        var i = 0
        for asset in editAssets {
            if asset.isSelected {
                i += 1
            }
        }
        return i
    }
    
    func didCancelPickPhoto(){
        editAssetsDict.removeAll()
        editAssets.removeAll()
        for asset in assets {
            editAssets.append(asset)
        }
        for asset in editAssets {
            if let image = asset.image {
                editAssetsDict[image] = asset
            }
            if let phasset = asset.photoAsset {
                editAssetsDict[phasset] = asset
            }
        }
    }
    
    func getAssetByPHAsset(asset:PHAsset)->DHImageAsset? {
        return assetsDict[asset]
    }
    
    func getAssetByImage(image:UIImage)->DHImageAsset? {
        return assetsDict[image]
    }
    
    func getEditAssetByPHAsset(asset:PHAsset)->DHImageAsset? {
        return editAssetsDict[asset]
    }
    
    func getEditAssetByImage(image:UIImage)->DHImageAsset? {
        return editAssetsDict[image]
    }
    

    
    func commit() {
        editAssetsDict.removeAll()
        assetsDict.removeAll()
        assets.removeAll()
        
        for asset in editAssets {
            if asset.isSelected {
                assets.append(asset)
            }
        }
        editAssets.removeAll()
        for asset in assets {
            editAssets.append(asset)
        }
        for asset in assets {
            if let image = asset.image {
                assetsDict[image] = asset
                editAssetsDict[image] = asset
            }
            if let phasset = asset.photoAsset {
                assetsDict[phasset] = asset
                editAssetsDict[phasset] = asset
            }
        }
    }
}

class DHImageAsset:NSObject {
    var image:UIImage?
    var photoAsset:PHAsset?
    var isOriginalPhoto:Bool = false
    
    var isSelected:Bool = false // property for DHPreviewController
}

