//
//  DHPhotoPickerController.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/14/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import UIKit
import Photos
import SnapKit

class DHPhotoPickerController: UIViewController {

    let HEIGHT_BOTTOM_TOOLBAR:CGFloat = 50
    
    var dataSource:DHImagePickerDataSource
    private var kvoContext = 0
    
    var assetsFetchResult:PHFetchResult
    var assetCollection:PHAssetCollection
    
    var collectionView:UICollectionView!
    var bottomToolbar:UIView!
    var previewBtn:UIButton!
    var doneBtn:UIButton!
    var countLabel:UILabel!
    
    var imageManager:PHCachingImageManager
    var previousPreheatRect:CGRect!
    
    static var AssetGridThumbnailSize:CGSize!
    lazy var imageScale = UIScreen.mainScreen().scale
    
    private weak var actionHandler:ActionHandler?
    
    init(dataSource:DHImagePickerDataSource, assetCollection:PHAssetCollection, assetsFetchResult:PHFetchResult) {
        self.dataSource = dataSource
        self.assetsFetchResult = assetsFetchResult
        self.assetCollection = assetCollection
        imageManager = PHCachingImageManager()
        super.init(nibName: nil, bundle: nil)
        self.actionHandler = self
        resetCachedAssets()
        setupNavigationBar()
    }
    
    deinit {
        self.resetCachedAssets()
        print(#function, "\(self)")
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = assetCollection.localizedTitle
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "cancel".localized, style: .Plain, target: self, action: #selector(onCancelClick))
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        let numItemsPerRow:CGFloat = 4
        let marginItem:CGFloat = 3
        let marginEdgeHorizontal:CGFloat = 3.5
        let marginEdgeVertical:CGFloat = 3
        
        let itemWH:CGFloat = (self.view.width - 2 * marginEdgeHorizontal - (numItemsPerRow - 1) * marginItem) / numItemsPerRow
        layout.itemSize = CGSizeMake(itemWH, itemWH)
        layout.minimumInteritemSpacing = marginItem
        layout.minimumLineSpacing = marginEdgeVertical
        DHPhotoPickerController.AssetGridThumbnailSize = CGSizeMake(layout.itemSize.width * imageScale, layout.itemSize.height)
        collectionView = UICollectionView(frame: CGRectMake(marginEdgeHorizontal, marginEdgeVertical, self.view.width - 2 * marginEdgeHorizontal, self.view.bounds.height - 44 - 20 - 2 * marginEdgeVertical - HEIGHT_BOTTOM_TOOLBAR), collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(DHPhotoPickerCell.self, forCellWithReuseIdentifier: "cell")
        
        let bottomIndexPath = NSIndexPath(forItem: assetsFetchResult.count - 1, inSection: 0)
        collectionView.scrollToItemAtIndexPath(bottomIndexPath, atScrollPosition: .Bottom, animated: false)
        view.addSubview(collectionView)
    }
    
//    func setupBottomToolbar() {
//        bottomToolbar = UIView()
//        bottomToolbar.frame = CGRectMake(0, self.view.bounds.height - 44 - 20 - HEIGHT_BOTTOM_TOOLBAR, self.view.bounds.width, HEIGHT_BOTTOM_TOOLBAR)
//        bottomToolbar.backgroundColor = StyleHelper.getTableViewBackgroundColor()
//        self.view.addSubview(bottomToolbar)
//        
//        let topSeparator = UIView()
//        topSeparator.backgroundColor = StyleHelper.getSeparatorColor()
//        topSeparator.frame = CGRectMake(0, 0, self.view.bounds.width, 1 / imageScale)
//        bottomToolbar.addSubview(topSeparator)
//        
//        let FONT_COLOR = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//        previewBtn = UIButton()
//        previewBtn.frame = CGRectMake(0, 0, 50, 22)
//        previewBtn.setTitle("预览", forState: .Normal)
//        previewBtn.setTitleColor(FONT_COLOR,  forState: .Normal)
//        previewBtn.setTitleColor(FONT_COLOR.colorWithAlphaComponent(0.5), forState: .Disabled)
//        previewBtn.titleLabel?.font = StyleHelper.getAppFontWithSize(18)
//        bottomToolbar.addSubview(previewBtn)
//        previewBtn.sizeToFit()
//        previewBtn.centerY = bottomToolbar.height / 2
//        previewBtn.x = 15
//        previewBtn.enabled = false
//        previewBtn.addTarget(.TouchUpInside) { [unowned self] in
//            self.onPreviewClick()
//        }
//        
//        doneBtn = UIButton()
//        doneBtn.frame = CGRectMake(0, 0, 50, 22)
//        doneBtn.setTitle("完成",forState: .Normal)
//        doneBtn.setTitleColor(StyleHelper.getAppThemeColor(),  forState: .Normal)
//        doneBtn.titleLabel?.font = StyleHelper.getAppFontWithSize(18)
//        bottomToolbar.addSubview(doneBtn)
//        doneBtn.sizeToFit()
//        doneBtn.centerY = bottomToolbar.height / 2
//        doneBtn.right = bottomToolbar.width - 15
//        doneBtn.addTarget(.TouchUpInside) { [unowned self] in
//            self.onDoneClick()
//        }
//        
//        countLabel = UILabel()
//        countLabel.frame = CGRectMake(0, 0, 19, 19)
//        countLabel.font = StyleHelper.getAppFontWithSize(16)
//        countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
//        countLabel.layer.cornerRadius = countLabel.width / 2
//        countLabel.layer.masksToBounds = true
//        countLabel.textColor = UIColor.whiteColor()
//        countLabel.textAlignment = .Center
//        countLabel.text = "0"
//        bottomToolbar.addSubview(countLabel)
//        countLabel.centerY = doneBtn.centerY
//        countLabel.right = doneBtn.left - 5
//        
//        refreshBottomBar()
//    }
    
    func setupBottomToolbar() {
        bottomToolbar = UIButton()
        bottomToolbar.frame = CGRectMake(0, self.view.bounds.height - 44 - 20 - HEIGHT_BOTTOM_TOOLBAR, self.view.bounds.width, HEIGHT_BOTTOM_TOOLBAR)
        self.view.addSubview(bottomToolbar)
        
        let btn = bottomToolbar as! UIButton
        btn.setTitle("done".localized, forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        btn.setTitleColor(UIColor.whiteColor().colorWithAlphaComponent(0.5), forState: UIControlState.Highlighted)
        btn.setBackgroundColor(UIColor.whiteColor(), forUIControlState: .Normal)
        if let color = (self.navigationController as? DHImagePickerController)?.bottomBarColor {
            btn.setBackgroundColor(color, forUIControlState: UIControlState.Normal)
        } else {
            btn.setBackgroundColor(StyleHelper.getAppThemeColor(), forUIControlState: UIControlState.Normal)
        }
        
        if let color = (self.navigationController as? DHImagePickerController)?.bottomBarHighLightedColor {
            btn.setBackgroundColor(color, forUIControlState: UIControlState.Highlighted)
        } else if let color = (self.navigationController as? DHImagePickerController)?.bottomBarColor {
            btn.setBackgroundColor(color, forUIControlState: UIControlState.Highlighted)
            
        } else {
            btn.setBackgroundColor(StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.9), forUIControlState: UIControlState.Highlighted)
        }
        
        btn.addTarget(.TouchUpInside) { [unowned self] in
            self.onDoneClick()
        }
        
        countLabel = UILabel()
        countLabel.frame = CGRectMake(0, 0, 26, 26)
        countLabel.font = StyleHelper.getAppFontWithSize(16)
        countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
        countLabel.layer.cornerRadius = countLabel.width / 2
        countLabel.layer.masksToBounds = true
        countLabel.textColor = StyleHelper.getAppThemeColor()
        countLabel.textAlignment = .Center
        countLabel.text = "0"
        countLabel.centerY = btn.height / 2
        countLabel.left = 15 * ScaleFactor.VERTICAL
        bottomToolbar.addSubview(countLabel)
        
        refreshBottomBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        setupCollectionView()
        setupBottomToolbar()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let indexPaths = self.collectionView.indexPathsForVisibleItems()
        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
        refreshBottomBar() 
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func refreshBottomBar() {
        if dataSource.editAssets.count > 0 {
            countLabel.backgroundColor = UIColor.whiteColor()
        } else {
            countLabel.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        }
        countLabel.text = String(dataSource.getEditAssetsSelectedCount())
    }
    
//    func refreshBottomBar() {
//        if dataSource.editAssets.count > 0 {
//            previewBtn.enabled = true
//            countLabel.backgroundColor = StyleHelper.getAppThemeColor()
//        } else {
//            previewBtn.enabled = false
//            countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
//        }
//        countLabel.text = String(dataSource.getEditAssetsSelectedCount())
//    }
}


// Asset Caching
extension DHPhotoPickerController {
    
    func resetCachedAssets() {
        self.imageManager.stopCachingImagesForAllAssets()
        self.previousPreheatRect = CGRectZero
    }
    
    func updateCachedAssets() {
        let isViewVisible = (self.isViewLoaded() && self.view.window != nil)
        if !isViewVisible {
            return
        }
        
        var preheatRect = self.collectionView.bounds;
        preheatRect = CGRectInset(preheatRect, 0, -0.5 * CGRectGetHeight(preheatRect))
        
        let delta = abs(CGRectGetMidY(preheatRect) - CGRectGetMidY(self.previousPreheatRect))
        if delta > self.collectionView.bounds.height / 3 {
            var addedIndexPaths = [NSIndexPath]()
            var removedIndexPaths = [NSIndexPath]()
            
            self.computeDifferenceBetweenRect(self.previousPreheatRect, andRect: preheatRect, removedHandler: { [unowned self] (removedRect) in
                    let indexPaths = self.indexPathsForElementsInRect(removedRect)
                    if indexPaths != nil {
                        removedIndexPaths.appendContentsOf(indexPaths!)
                    }
                }, addedHandler: { [unowned self] (addedRect) in
                    let indexPaths = self.indexPathsForElementsInRect(addedRect)
                    if indexPaths != nil {
                        addedIndexPaths.appendContentsOf(indexPaths!)
                    }
            })
            
            if let assetsToStartCaching = assetsAtIndexPaths(addedIndexPaths) {
                self.imageManager.startCachingImagesForAssets(assetsToStartCaching, targetSize: DHPhotoPickerController.AssetGridThumbnailSize, contentMode: .AspectFill, options: nil)
            }
            
            if let assetsToStopCaching = assetsAtIndexPaths(removedIndexPaths) {
                self.imageManager.stopCachingImagesForAssets(assetsToStopCaching, targetSize: DHPhotoPickerController.AssetGridThumbnailSize, contentMode: .AspectFill, options: nil)
            }
            
            self.previousPreheatRect = preheatRect
        }
    }
    
    func assetsAtIndexPaths(indexPaths:[NSIndexPath])->[PHAsset]? {
        if (indexPaths.count == 0) { return nil; }
        
        var assets = [PHAsset]()
        assets.reserveCapacity(indexPaths.count)
        for indexPath in indexPaths {
            let asset = self.assetsFetchResult[indexPath.item];
            assets.append(asset as! PHAsset)
        }
        
        return assets
    }
    
    func indexPathsForElementsInRect(rect:CGRect)->[NSIndexPath]? {
        let allLayoutAttributes = self.collectionView.collectionViewLayout.layoutAttributesForElementsInRect(rect)
        if allLayoutAttributes != nil && allLayoutAttributes!.count == 0 {
            return nil
        }
        
        var indexPaths = [NSIndexPath]()
        indexPaths.reserveCapacity(allLayoutAttributes!.count)
        for layoutAttribute in allLayoutAttributes! {
            let indexPath = layoutAttribute.indexPath
            indexPaths.append(indexPath)
        }
        
        return indexPaths
    }
    
    func computeDifferenceBetweenRect(oldRect:CGRect, andRect newRect:CGRect, removedHandler:(CGRect)->Void, addedHandler:(CGRect)->Void) {
        if CGRectIntersectsRect(newRect, oldRect) {
            let oldMaxY = CGRectGetMaxY(oldRect)
            let oldMinY = CGRectGetMinY(oldRect)
            let newMaxY = CGRectGetMaxY(newRect)
            let newMinY = CGRectGetMinY(newRect)
            
            if (newMaxY > oldMaxY) {
                let rectToAdd = CGRectMake(newRect.origin.x, oldMaxY, newRect.size.width, (newMaxY - oldMaxY))
                addedHandler(rectToAdd)
            }
            
            if (oldMinY > newMinY) {
                let rectToAdd = CGRectMake(newRect.origin.x, newMinY, newRect.size.width, (oldMinY - newMinY))
                addedHandler(rectToAdd)
            }
            
            if (newMaxY < oldMaxY) {
                let rectToRemove = CGRectMake(newRect.origin.x, newMaxY, newRect.size.width, (oldMaxY - newMaxY))
                removedHandler(rectToRemove)
            }
            
            if (oldMinY < newMinY) {
                let rectToRemove = CGRectMake(newRect.origin.x, oldMinY, newRect.size.width, (newMinY - oldMinY))
                removedHandler(rectToRemove)
            }
        } else {
            addedHandler(newRect)
            removedHandler(oldRect)
        }
    }
}

extension DHPhotoPickerController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.assetsFetchResult.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DHPhotoPickerCell
        let asset = self.assetsFetchResult[indexPath.item] as! PHAsset
        cell.representedAssetIdentifier = asset.localIdentifier
        
        if let asset = self.dataSource.getEditAssetByPHAsset(asset) {
            cell.imageSelected(asset.isSelected)
        } else {
            cell.imageSelected(false)
        }
        
        let opts = PHImageRequestOptions()
        opts.networkAccessAllowed = true
        self.imageManager.requestImageForAsset(asset, targetSize:DHPhotoPickerController.AssetGridThumbnailSize, contentMode: .AspectFill, options: opts) { (image, info) in
            if cell.representedAssetIdentifier == asset.localIdentifier {
                if image != nil {
                    cell.setThumbNailImage(image!)
                }
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let asset = assetsFetchResult[indexPath.item] as! PHAsset
        
        if self.dataSource.getEditAssetByPHAsset(asset) != nil {
            self.dataSource.removeAssetByPHAsset(asset)
        } else {
            if self.dataSource.getEditAssetsSelectedCount() >= self.dataSource.maxCount {
                return
            }
            self.dataSource.addAsset(asset, isOriginalPhoto: false)
        }
        
        self.collectionView.reloadItemsAtIndexPaths([indexPath])
        _ = self.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! DHPhotoPickerCell
//        AnimationHelper.mimicImageClick(cell.topRightSelector)
        
        self.refreshBottomBar()
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.updateCachedAssets()
    }
}


// Action Handler
private protocol ActionHandler:class {
    func onPreviewClick()
    func onDoneClick()
}

extension DHPhotoPickerController:ActionHandler {
    func onPreviewClick() {
        let vc = DHPreviewController()
        vc.pickerDataSource = self.dataSource
        vc.allowPickingOriginalPhoto = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func onDoneClick() {
        self.dataSource.commit()
       
        if let navVC = self.navigationController as? DHImagePickerController {
            if navVC.doneClosure != nil {
                navVC.doneClosure!()
                return
            }
            
            if let delegate = navVC.dhipcDelegate {
                delegate.didFinishPickPhoto(navVC)
                return
            }
        }
        self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func onCancelClick() {
        self.dataSource.didCancelPickPhoto()
        
        if let navVC = self.navigationController as? DHImagePickerController {
            if navVC.cancelClosure != nil {
                navVC.cancelClosure!()
                return
            }
            
            if let delegate = navVC.dhipcDelegate {
                delegate.didCancelPickPhoto(navVC)
                return
            }
        }
        self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
    }
}

class DHPhotoPickerCell:UICollectionViewCell {
    
    var topRightSelector:UIImageView!
    var thumbnail:UIImageView!
    var representedAssetIdentifier:String!
    var thumbnailImage:UIImage!
    
    let MARGIN_SELECTOR:CGFloat = 2
    
    func imageSelected(selected:Bool) {
        if selected {
            topRightSelector.image = UIImage(named: "dhpicker_photo_selected.png")!
        } else {
            topRightSelector.image = UIImage(named: "dhpicker_photo_unselected.png")!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let topRightImage = UIImage(named: "dhpicker_photo_unselected.png")!
        topRightSelector = UIImageView()
        topRightSelector.frame = CGRectMake(0, 0, topRightImage.size.width, topRightImage.size.height)
        thumbnail = UIImageView(frame:CGRectZero)
        thumbnail.contentMode = .ScaleAspectFill
        thumbnail.clipsToBounds = true
        self.addSubview(thumbnail)
        thumbnail.snp_makeConstraints { (make) in
            make.size.equalTo(self)
        }
        self.addSubview(topRightSelector)
    }
    
    override func layoutSubviews() {
//        thumbnail.frame = contentView.frame
        topRightSelector.bottom = contentView.height - MARGIN_SELECTOR
        topRightSelector.right = contentView.width - MARGIN_SELECTOR
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.thumbnail.image = nil
    }
    
    func setThumbNailImage(image:UIImage?) {
        self.thumbnailImage = image
        self.thumbnail.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}





