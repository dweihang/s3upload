////
////  DHDeletePhotoController.swift
////  s3upload
////
////  Created by hy110831 on 7/22/16.
////  Copyright © 2016 hy110831. All rights reserved.
////
//
//import Foundation
//import UIKit
//import Photos
//import SnapKit
//
//class DHDeletePhotoController: UIViewController {
//    
//    let HEIGHT_BOTTOM_TOOLBAR:CGFloat = 50
//    
//    var dataSource:DHImagePickerDataSource
//    private var kvoContext = 0
//    
//    var collectionView:UICollectionView!
//    var bottomToolbar:UIView!
//    var previewBtn:UIButton!
//    var doneBtn:UIButton!
//    var countLabel:UILabel!
//    
//    var previousPreheatRect:CGRect!
//    
//    static var AssetGridThumbnailSize:CGSize!
//    lazy var imageScale = UIScreen.mainScreen().scale
//    
//    private weak var actionHandler:ActionHandler?
//    
//    init(dataSource:DHImagePickerDataSource) {
//        self.dataSource = dataSource
//        super.init(nibName: nil, bundle: nil)
//        self.actionHandler = self
//        setupNavigationBar()
//    }
//    
//    deinit {
//        print(#function, "\(self)")
//    }
//    
//    func setupNavigationBar() {
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "cancel".localized, style: .Plain, target: self, action: #selector(onCancelClick))
//    }
//    
//    func setupCollectionView() {
//        let layout = UICollectionViewFlowLayout()
//        let numItemsPerRow:CGFloat = 4
//        let marginItem:CGFloat = 3
//        let marginEdgeHorizontal:CGFloat = 3.5
//        let marginEdgeVertical:CGFloat = 3
//        
//        let itemWH:CGFloat = (self.view.width - 2 * marginEdgeHorizontal - (numItemsPerRow - 1) * marginItem) / numItemsPerRow
//        layout.itemSize = CGSizeMake(itemWH, itemWH)
//        layout.minimumInteritemSpacing = marginItem
//        layout.minimumLineSpacing = marginEdgeVertical
//        DHDeletePhotoController.AssetGridThumbnailSize = CGSizeMake(layout.itemSize.width * imageScale, layout.itemSize.height)
//        collectionView = UICollectionView(frame: CGRectMake(marginEdgeHorizontal, marginEdgeVertical, self.view.width - 2 * marginEdgeHorizontal, self.view.height - 2 * marginEdgeVertical), collectionViewLayout: layout)
//        collectionView.collectionViewLayout = layout
//        collectionView.backgroundColor = UIColor.whiteColor()
//        collectionView.height = self.view.bounds.height - 44 - 20 - 2 - HEIGHT_BOTTOM_TOOLBAR
//        
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        collectionView.registerClass(DHPhotoPickerCell.self, forCellWithReuseIdentifier: "cell")
//        
//        view.addSubview(collectionView)
//    }
//    
//    //    func setupBottomToolbar() {
//    //        bottomToolbar = UIView()
//    //        bottomToolbar.frame = CGRectMake(0, self.view.bounds.height - 44 - 20 - HEIGHT_BOTTOM_TOOLBAR, self.view.bounds.width, HEIGHT_BOTTOM_TOOLBAR)
//    //        bottomToolbar.backgroundColor = StyleHelper.getTableViewBackgroundColor()
//    //        self.view.addSubview(bottomToolbar)
//    //
//    //        let topSeparator = UIView()
//    //        topSeparator.backgroundColor = StyleHelper.getSeparatorColor()
//    //        topSeparator.frame = CGRectMake(0, 0, self.view.bounds.width, 1 / imageScale)
//    //        bottomToolbar.addSubview(topSeparator)
//    //
//    //        let FONT_COLOR = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//    //        previewBtn = UIButton()
//    //        previewBtn.frame = CGRectMake(0, 0, 50, 22)
//    //        previewBtn.setTitle("预览", forState: .Normal)
//    //        previewBtn.setTitleColor(FONT_COLOR,  forState: .Normal)
//    //        previewBtn.setTitleColor(FONT_COLOR.colorWithAlphaComponent(0.5), forState: .Disabled)
//    //        previewBtn.titleLabel?.font = StyleHelper.getAppFontWithSize(18)
//    //        bottomToolbar.addSubview(previewBtn)
//    //        previewBtn.sizeToFit()
//    //        previewBtn.centerY = bottomToolbar.height / 2
//    //        previewBtn.x = 15
//    //        previewBtn.enabled = false
//    //        previewBtn.addTarget(.TouchUpInside) { [unowned self] in
//    //            self.onPreviewClick()
//    //        }
//    //
//    //        doneBtn = UIButton()
//    //        doneBtn.frame = CGRectMake(0, 0, 50, 22)
//    //        doneBtn.setTitle("完成",forState: .Normal)
//    //        doneBtn.setTitleColor(StyleHelper.getAppThemeColor(),  forState: .Normal)
//    //        doneBtn.titleLabel?.font = StyleHelper.getAppFontWithSize(18)
//    //        bottomToolbar.addSubview(doneBtn)
//    //        doneBtn.sizeToFit()
//    //        doneBtn.centerY = bottomToolbar.height / 2
//    //        doneBtn.right = bottomToolbar.width - 15
//    //        doneBtn.addTarget(.TouchUpInside) { [unowned self] in
//    //            self.onDoneClick()
//    //        }
//    //
//    //        countLabel = UILabel()
//    //        countLabel.frame = CGRectMake(0, 0, 19, 19)
//    //        countLabel.font = StyleHelper.getAppFontWithSize(16)
//    //        countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
//    //        countLabel.layer.cornerRadius = countLabel.width / 2
//    //        countLabel.layer.masksToBounds = true
//    //        countLabel.textColor = UIColor.whiteColor()
//    //        countLabel.textAlignment = .Center
//    //        countLabel.text = "0"
//    //        bottomToolbar.addSubview(countLabel)
//    //        countLabel.centerY = doneBtn.centerY
//    //        countLabel.right = doneBtn.left - 5
//    //
//    //        refreshBottomBar()
//    //    }
//    
//    func setupBottomToolbar() {
//        bottomToolbar = UIButton()
//        bottomToolbar.frame = CGRectMake(0, self.view.bounds.height - 44 - 20 - HEIGHT_BOTTOM_TOOLBAR, self.view.bounds.width, HEIGHT_BOTTOM_TOOLBAR)
//        self.view.addSubview(bottomToolbar)
//        
//        let btn = bottomToolbar as! UIButton
//        btn.setTitle("Done", forState: UIControlState.Normal)
//        btn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
//        btn.setTitleColor(UIColor.whiteColor().colorWithAlphaComponent(0.5), forState: UIControlState.Highlighted)
//        btn.setBackgroundColor(UIColor.whiteColor(), forUIControlState: .Normal)
//        if let color = (self.navigationController as? DHImagePickerController)?.bottomBarColor {
//            btn.setBackgroundColor(color, forUIControlState: UIControlState.Normal)
//        } else {
//            btn.setBackgroundColor(StyleHelper.getAppThemeColor(), forUIControlState: UIControlState.Normal)
//        }
//        
//        if let color = (self.navigationController as? DHImagePickerController)?.bottomBarHighLightedColor {
//            btn.setBackgroundColor(color, forUIControlState: UIControlState.Highlighted)
//        } else if let color = (self.navigationController as? DHImagePickerController)?.bottomBarColor {
//            btn.setBackgroundColor(color, forUIControlState: UIControlState.Highlighted)
//            
//        } else {
//            btn.setBackgroundColor(StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.9), forUIControlState: UIControlState.Highlighted)
//        }
//        
//        btn.addTarget(.TouchUpInside) { [unowned self] in
//            self.onDoneClick()
//        }
//        
//        countLabel = UILabel()
//        countLabel.frame = CGRectMake(0, 0, 26, 26)
//        countLabel.font = StyleHelper.getAppFontWithSize(16)
//        countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
//        countLabel.layer.cornerRadius = countLabel.width / 2
//        countLabel.layer.masksToBounds = true
//        countLabel.textColor = StyleHelper.getAppThemeColor()
//        countLabel.textAlignment = .Center
//        countLabel.text = "0"
//        countLabel.centerY = btn.height / 2
//        countLabel.left = 15 * ScaleFactor.VERTICAL
//        bottomToolbar.addSubview(countLabel)
//        
//        refreshBottomBar()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.view.backgroundColor = UIColor.whiteColor()
//        setupCollectionView()
//        setupBottomToolbar()
//    }
//    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        let indexPaths = self.collectionView.indexPathsForVisibleItems()
//        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
//        refreshBottomBar()
//    }
//    
//    override func viewWillDisappear(animated: Bool) {
//        super.viewWillDisappear(animated)
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//    
//    func refreshBottomBar() {
//        if dataSource.editAssets.count > 0 {
//            countLabel.backgroundColor = UIColor.whiteColor()
//        } else {
//            countLabel.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
//        }
//        countLabel.text = String(dataSource.getEditAssetsSelectedCount())
//    }
//    
//    //    func refreshBottomBar() {
//    //        if dataSource.editAssets.count > 0 {
//    //            previewBtn.enabled = true
//    //            countLabel.backgroundColor = StyleHelper.getAppThemeColor()
//    //        } else {
//    //            previewBtn.enabled = false
//    //            countLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
//    //        }
//    //        countLabel.text = String(dataSource.getEditAssetsSelectedCount())
//    //    }
//}
//
//// MARK: Collection View
//extension DHDeletePhotoController: UICollectionViewDelegate, UICollectionViewDataSource {
//    
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.assetsFetchResult.count
//    }
//    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! DHPhotoPickerCell
//        let asset = self.assetsFetchResult[indexPath.item] as! PHAsset
//        cell.representedAssetIdentifier = asset.localIdentifier
//        
//        if let asset = self.dataSource.getEditAssetByPHAsset(asset) {
//            cell.imageSelected(!asset.isSelected)
//        } else {
//            cell.imageSelected(false)
//        }
//        
//        
//        let opts = PHImageRequestOptions()
//        opts.networkAccessAllowed = true
//        opts.deliveryMode = .Opportunistic
//        PHImageManager.defaultManager()
//         .requestImageForAsset(asset, targetSize:DHDeletePhotoController.AssetGridThumbnailSize, contentMode: .AspectFill, options: opts) { (image, info) in
//            if cell.representedAssetIdentifier == asset.localIdentifier {
//                if image != nil {
//                    cell.setThumbNailImage(image!)
//                }
//            }
//        }
//        
//        return cell
//    }
//    
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        let asset = assetsFetchResult[indexPath.item] as! PHAsset
//        
//        if self.dataSource.getEditAssetByPHAsset(asset) != nil {
//            self.dataSource.removeAssetByPHAsset(asset)
//        } else {
//            if self.dataSource.getEditAssetsSelectedCount() >= self.dataSource.maxCount {
//                return
//            }
//            self.dataSource.addAsset(asset, isOriginalPhoto: false)
//        }
//        
//        self.collectionView.reloadItemsAtIndexPaths([indexPath])
//        _ = self.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as! DHPhotoPickerCell
//        //        AnimationHelper.mimicImageClick(cell.topRightSelector)
//        
//        self.refreshBottomBar()
//        
//    }
//}
//
//
//// MARK: Action Handler
//private protocol ActionHandler:class {
//    func onPreviewClick()
//    func onDoneClick()
//}
//
//extension DHDeletePhotoController:ActionHandler {
//    func onPreviewClick() {
//        let vc = DHPreviewController()
//        vc.pickerDataSource = self.dataSource
//        vc.allowPickingOriginalPhoto = true
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//    
//    func onDoneClick() {
//        self.dataSource.commit()
//        
//        if let navVC = self.navigationController as? DHImagePickerController {
//            if navVC.doneClosure != nil {
//                navVC.doneClosure!()
//                return
//            }
//            
//            if let delegate = navVC.dhipcDelegate {
//                delegate.didFinishPickPhoto(navVC)
//                return
//            }
//        }
//        self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    func onCancelClick() {
//        self.dataSource.didCancelPickPhoto()
//        
//        if let navVC = self.navigationController as? DHImagePickerController {
//            if navVC.cancelClosure != nil {
//                navVC.cancelClosure!()
//                return
//            }
//            
//            if let delegate = navVC.dhipcDelegate {
//                delegate.didCancelPickPhoto(navVC)
//                return
//            }
//        }
//        self.presentingViewController!.dismissViewControllerAnimated(true, completion: nil)
//    }
//}
//
//
//
//
//
//
