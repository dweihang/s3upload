//
//  DHPreviewController.swift
//  XFBConsumer
//
//  Created by hy110831 on 4/17/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import UIKit
import Photos

class DHPreviewController: UIViewController {
    
    var pickerDataSource:DHImagePickerDataSource!
    
    var allowPickingOriginalPhoto:Bool = false
    var isOriginalPhotoSelected:Bool = false
    var currentIndex:Int = 0
    var isNaviBarHidden:Bool = false
    
    var collectionView:UICollectionView!
    
    var naviBar:UIView!
    var backButton:UIButton!
    var selectButton:UIButton!
    
    var toolBar:UIView!
    var doneButton:UIButton!
    var numberLabel: UILabel!
    var originalPhotoButton:UIButton!
    var originalPhotoLabel:UILabel!
    
    var doneClosure:((Void)->(Void))?

    private weak var actionHandler:ActionHandler?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        actionHandler = self
        setupCollectionView()
        setupNavBar()
        setupBottomBar()
    }
    
    deinit {
        print(#function, "\(self)")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        if currentIndex != 0 {
            collectionView.setContentOffset(CGPointMake((self.view.width) * CGFloat(currentIndex), 0), animated: false)
        }
        self.refreshNaviBarAndBottomBarState()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func drawOriginalButtonSelectedImage(size:CGSize, borderColor:UIColor, selectedColor:UIColor)->UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
        let ctx = UIGraphicsGetCurrentContext()
        CGContextSaveGState(ctx)
        
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        borderColor.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha)
        
        let padding:CGFloat = 4
        let rect = CGRectMake(padding, padding, size.width - padding * 2, size.height - padding * 2)
        CGContextSetFillColorWithColor(ctx, selectedColor.CGColor)
        CGContextFillEllipseInRect(ctx, rect)
        
        let borderWidth:CGFloat = 1.5
        CGContextSetLineWidth(ctx, borderWidth)
        CGContextSetRGBStrokeColor(ctx, fRed, fGreen, fBlue, fAlpha)
        CGContextAddArc(ctx, size.width / 2, size.height / 2, size.width / 2  - borderWidth, 0.0, CGFloat(M_PI) * 2, 1)
        CGContextStrokePath(ctx)
        
        CGContextRestoreGState(ctx)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img
    }
    
    private func drawOriginalButtonUnselectedImage(size:CGSize, borderColor:UIColor)->UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
        let ctx = UIGraphicsGetCurrentContext()
        CGContextSaveGState(ctx)
        
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        borderColor.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha)
        
        let borderWidth:CGFloat = 1.5
        CGContextSetLineWidth(ctx, borderWidth)
        CGContextSetRGBStrokeColor(ctx, fRed, fGreen, fBlue, fAlpha)
        CGContextAddArc(ctx, size.width / 2, size.height / 2, size.width / 2 - borderWidth, 0.0, CGFloat(M_PI) * 2, 1)
        CGContextStrokePath(ctx)
        
        CGContextRestoreGState(ctx)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return img
    }
    
    func setupNavBar() {
        naviBar = UIView(frame:CGRectMake(0, 0, self.view.width, 64))
        naviBar.backgroundColor = UIColor(red:(34/255.0), green:(34/255.0), blue:(34/255.0), alpha:1.0)
        
        backButton = UIButton(frame:CGRectMake(10, 10, 44, 44))
        backButton.setImage(UIImage(named:"dhpicker_nav_back.png"),forState:.Normal)
        backButton.setTitleColor(UIColor.whiteColor(), forState:.Normal)

        backButton.addTarget(.TouchUpInside) { [unowned self] in
            self.actionHandler?.onBackClick()
        }
        
        selectButton = UIButton(frame:CGRectMake(self.view.width - 54, 10, 42, 42))
        selectButton.setImage(UIImage(named:"dhpicker_preview_unselected.png"), forState:.Normal)
        selectButton.setImage(UIImage(named:"dhpicker_preview_selected.png"), forState:.Selected)
        selectButton.addTarget(.TouchUpInside) { [unowned self] in
            self.actionHandler?.onSelectClick()
        }
        
        naviBar.addSubview(selectButton)
        naviBar.addSubview(backButton)
        self.view.addSubview(naviBar)
    }
    
    func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        layout.itemSize = CGSizeMake(self.view.width, self.view.height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView = UICollectionView(frame: CGRectMake(0, 0, self.view.width, self.view.height), collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.blackColor()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.pagingEnabled = true
        collectionView.scrollsToTop = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.contentOffset = CGPointMake(0, 0)
        collectionView.contentSize = CGSizeMake(self.view.width * CGFloat(self.pickerDataSource.assets.count), self.view.height);
        self.view.addSubview(collectionView)
        collectionView.registerClass(PreviewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    func setupBottomBar() {
        toolBar = UIView(frame:CGRectMake(0, self.view.height - 44, self.view.width, 44))
        let rgb:CGFloat = 34 / 255.0;
        toolBar.backgroundColor = UIColor(red:rgb, green:rgb, blue:rgb, alpha:1.0)
        toolBar.alpha = 0.7
        
        if (self.allowPickingOriginalPhoto) {
            originalPhotoButton = UIButton()
            originalPhotoButton.frame = CGRectMake(5, 0, 120, 44)
            originalPhotoButton.imageEdgeInsets = UIEdgeInsetsMake(0, -8, 0, 0)
            originalPhotoButton.contentEdgeInsets = UIEdgeInsetsMake(0, -50, 0, 0)
            originalPhotoButton.backgroundColor = UIColor.clearColor()
            originalPhotoButton.titleLabel?.font = UIFont.systemFontOfSize(13)
            originalPhotoButton.setTitle("原图", forState:UIControlState.Normal)
            originalPhotoButton.setTitleColor(UIColor.lightGrayColor(), forState:UIControlState.Normal)
            originalPhotoButton.setTitleColor(UIColor.whiteColor(), forState:UIControlState.Selected)
            originalPhotoButton.setImage(drawOriginalButtonUnselectedImage(CGSizeMake(20, 20), borderColor: UIColor(red:196/255, green:196/255, blue:196/255, alpha:1.0)), forState:UIControlState.Normal)
            originalPhotoButton.setImage(drawOriginalButtonSelectedImage(CGSizeMake(20, 20), borderColor: UIColor(red:196/255, green:196/255, blue:196/255, alpha:1.0), selectedColor: StyleHelper.getAppThemeColor()), forState:UIControlState.Selected)
            
            originalPhotoLabel = UILabel()
            originalPhotoLabel.frame = CGRectMake(60, 0, 70, 44)
            originalPhotoLabel.textAlignment = NSTextAlignment.Left
            originalPhotoLabel.font = UIFont.systemFontOfSize(13)
            originalPhotoLabel.textColor = UIColor.whiteColor()
            originalPhotoLabel.backgroundColor = UIColor.clearColor()
            originalPhotoButton.addSubview(originalPhotoLabel)
            if isOriginalPhotoSelected {
                self.showPhotoBytes()
            }
            originalPhotoButton.addTarget(.TouchUpInside, action: { [unowned self] in
                self.actionHandler?.onOriginalButtonClick()
            })
        }
        
        doneButton = UIButton(type: .Custom)
        doneButton.frame = CGRectMake(self.view.width - 44 - 12, 0, 44, 44)
        doneButton.titleLabel?.font = UIFont.systemFontOfSize(16)
        doneButton.addTarget(.TouchUpInside) { 
            [unowned self] in
            self.actionHandler?.onDoneClick()
        }
        doneButton.setTitle("done".localized, forState: .Normal)
        doneButton.setTitleColor(StyleHelper.getAppThemeColor(), forState: .Normal)
        
//        numberImageView = UIImageView(image: UIImage(named: "photo_number_icon"))
//        numberImageView.backgroundColor = UIColor.clearColor()
//        numberImageView.frame = CGRectMake(self.view.width - 56 - 24, 9, 26, 26)
//        numberImageView.hidden = self.pickerDataSource.assets.count <= 0
        
        numberLabel = UILabel()
        numberLabel.frame = CGRectMake(self.view.width - 56 - 24, 9, 26, 26)
        numberLabel.backgroundColor = StyleHelper.getAppThemeColor()
        numberLabel.layer.cornerRadius = numberLabel.frame.width / 2
        numberLabel.layer.masksToBounds = true
        numberLabel.font = UIFont.systemFontOfSize(16)
        numberLabel.textColor = UIColor.whiteColor()
        numberLabel.textAlignment = .Center
        numberLabel.text = "\(self.pickerDataSource.getEditAssetsSelectedCount())"
        if self.pickerDataSource.editAssets.count <= 0 {
            numberLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
        } else {
            numberLabel.backgroundColor = StyleHelper.getAppThemeColor()
        }
//        numberLabel.hidden = self.pickerDataSource.assets.count <= 0
//        numberLabel.backgroundColor = UIColor.clearColor()
        
        toolBar.addSubview(doneButton)
        if (allowPickingOriginalPhoto) {
            toolBar.addSubview(originalPhotoButton)
        }
//        toolBar.addSubview(numberImageView)
        toolBar.addSubview(numberLabel)
        self.view!.addSubview(toolBar)
    }
    
    func refreshNaviBarAndBottomBarState() {
        self.numberLabel.text = "\(self.pickerDataSource.getEditAssetsSelectedCount())"
//        self.numberImageView.hidden = (self.pickerDataSource.editAssets.count <= 0 || isNaviBarHidden)
//        self.numberLabel.hidden = (self.pickerDataSource.editAssets.count <= 0 || isNaviBarHidden)
        if self.pickerDataSource.editAssets.count <= 0 {
            numberLabel.backgroundColor = StyleHelper.getAppThemeColor().colorWithAlphaComponent(0.5)
        } else {
            numberLabel.backgroundColor = StyleHelper.getAppThemeColor()
        }
        
        if allowPickingOriginalPhoto {
            self.originalPhotoButton.selected = isOriginalPhotoSelected
            self.originalPhotoLabel.hidden = !originalPhotoButton.selected
            if isOriginalPhotoSelected {
                self.showPhotoBytes()
            }
            self.originalPhotoButton.hidden = false
            if isOriginalPhotoSelected {
                self.originalPhotoLabel.hidden = false
            }
        }
        
//        if self.pickerDataSource.[currentIndex] != nil {
//            selectButton.selected = true
//        } else {
//            selectButton.selected = false
//        }
        
        if self.pickerDataSource.editAssets[currentIndex].isSelected == true {
            selectButton.selected = true
        } else {
            selectButton.selected = false
        }

        if isNaviBarHidden {
            return
        }
    }
    
    func showPhotoBytes() {
        if let photoAsset = self.pickerDataSource.editAssets[currentIndex].photoAsset {
            let opts = PHImageRequestOptions()
            opts.networkAccessAllowed = true
            PHImageManager.defaultManager().requestImageDataForAsset(photoAsset, options: opts) { [unowned self] (data, _, _, _) in
                if let dat = data {
                    self.originalPhotoLabel.text = "(\(self.getBytesFromDataLength(dat.length)))"
                } else {
                    self.originalPhotoLabel.text = ""
                }
            }
        } else if let image = self.pickerDataSource.editAssets[currentIndex].image {
            if let data = UIImageJPEGRepresentation(image, 1) {
                self.originalPhotoLabel.text = "(\(data.length))"
            }
        }
    }
    
    func getBytesFromDataLength(dataLength: Int) -> String {
        var bytes: String
        if dataLength >= Int(0.1 * (1024 * 1024)) {
            bytes = String(format: "%0.1fM", Double(dataLength / 1024) / 1024.0)
        }
        else if dataLength >= 1024 {
            bytes = String(format: "%0.0fK", Double(dataLength) / 1024.0)
        }
        else {
            bytes = "\(dataLength)B"
        }
        
        return bytes
    }
}

extension DHPreviewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pickerDataSource.editAssets.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! PreviewCell
        if let phasset = self.pickerDataSource.editAssets[indexPath.row].photoAsset {
            cell.fetchPhotoWithAsset(phasset)
        }
        if let image = self.pickerDataSource.editAssets[indexPath.row].image {
            cell.imageView.image = image
            cell.resizeSubviews()
        }

        cell.singleTapBlock = { [unowned self] (cell) in
            self.isNaviBarHidden = !self.isNaviBarHidden
            self.naviBar.hidden = self.isNaviBarHidden
            self.toolBar.hidden = self.naviBar.hidden
        }

        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset
        currentIndex = Int(offSet.x) / Int(self.view.width)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        self.refreshNaviBarAndBottomBarState()
    }
}


// Action Handler
private protocol ActionHandler:class {
    func onSelectClick()
    func onBackClick()
    func onDoneClick()
    func onOriginalButtonClick()
}

extension DHPreviewController:ActionHandler {
    /**
     * 点击了是否选择的按钮
     */
    func onSelectClick() {
//        if self.pickerDataSource.editAssets[currentIndex] != nil {
//            self.pickerDataSource.editAssets[currentIndex] = nil
//        } else {
//            self.pickerDataSource.editAssets[currentIndex] = false
//        }
        if currentIndex < self.pickerDataSource.editAssets.count {
            self.pickerDataSource.editAssets[currentIndex].isSelected = !self.pickerDataSource.editAssets[currentIndex].isSelected
            self.selectButton.selected = self.pickerDataSource.editAssets[currentIndex].isSelected
            self.refreshNaviBarAndBottomBarState()
        }
        
    }
    
    func onOriginalButtonClick() {
        self.selectButton.selected = true
        
        if !self.originalPhotoButton.selected {
            for asset in self.pickerDataSource.editAssets {
                asset.isOriginalPhoto = true
            }
            self.isOriginalPhotoSelected = true
            self.originalPhotoButton.selected = true
            self.originalPhotoLabel.hidden = !self.originalPhotoButton.selected
            if isOriginalPhotoSelected {
                self.showPhotoBytes()
            }
        } else {
            for asset in self.pickerDataSource.editAssets {
                asset.isOriginalPhoto = false
            }
            self.isOriginalPhotoSelected = false
            self.originalPhotoButton.selected = false
            self.originalPhotoLabel.hidden = !self.originalPhotoButton.selected
        }
    }
    
    /**
     * 点击了返回
     */
    func onBackClick() {
        self.pickerDataSource.commit()
        if self.navigationController != nil {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    /**
     * 点击了确定按钮
     */
    func onDoneClick() {
        self.pickerDataSource.commit()
        
        if self.doneClosure != nil {
            self.doneClosure!()
            return
        }
        
        if let navVC = self.navigationController as? DHImagePickerController {
            if navVC.doneClosure != nil {
                navVC.doneClosure!()
                return
            }
            
            if let delegate = navVC.dhipcDelegate {
                delegate.didFinishPickPhoto(navVC)
                return
            }
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

class PreviewCell:UICollectionViewCell {
    var scrollView:UIScrollView!
    var imageView:UIImageView!
    var imageContainerView:UIView!
    
    var singleTapBlock:((PreviewCell)->(Void))?
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.blackColor()
        scrollView = UIScrollView()
        scrollView.frame = CGRectMake(0, 0, self.width, self.height)
        scrollView.bouncesZoom = true
        scrollView.maximumZoomScale = 2.5
        scrollView.minimumZoomScale = 1.0
        scrollView.multipleTouchEnabled = true
        scrollView.delegate = self
        scrollView.scrollsToTop = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        scrollView.delaysContentTouches = false
        scrollView.canCancelContentTouches = true
        scrollView.alwaysBounceVertical = false
        self.addSubview(scrollView)
            
        imageContainerView = UIView()
        imageContainerView.clipsToBounds = true
        scrollView.addSubview(imageContainerView)
            
        imageView = UIImageView()
        imageView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
        imageView.clipsToBounds = true
        imageContainerView.addSubview(imageView)
        
        self.addDoubleTapGestureRecognizerWithResponder { [unowned self] (tap) in
            if self.scrollView.zoomScale > 1.0 {
                self.scrollView.setZoomScale(1.0, animated: true)
            }
            else {
                let touchPoint: CGPoint = tap.locationInView(self.imageView)
                let newZoomScale: CGFloat = self.scrollView.maximumZoomScale
                let xsize: CGFloat = self.frame.size.width / newZoomScale
                let ysize: CGFloat = self.frame.size.height / newZoomScale
                self.scrollView.zoomToRect(CGRectMake(touchPoint.x - xsize / 2, touchPoint.y - ysize / 2, xsize, ysize), animated: true)
            }
        }
        
        self.addSingleTapGestureRecognizerWithResponder { [unowned self] (tap) in
            self.singleTapBlock?(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        self.singleTapBlock = nil
    }
    
    func fetchPhotoWithAsset(asset:PHAsset) {
        scrollView.setZoomScale(1.0, animated:true)
        
        let opts = PHImageRequestOptions()
        opts.networkAccessAllowed = true
        PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: CGSizeMake(self.width * UIScreen.mainScreen().scale, CGFloat(asset.pixelHeight) * UIScreen.mainScreen().scale / CGFloat(asset.pixelWidth) * self.width), contentMode: PHImageContentMode.AspectFit, options: opts) { [unowned self] (image, info) in
            self.imageView.image = image
            self.resizeSubviews()
        }
    }
    
    func resizeSubviews() {
        imageContainerView.origin = CGPointZero
        imageContainerView.width = self.width
    
        if let image = imageView.image {
            if (image.size.height / image.size.width > self.height / self.width) {
                imageContainerView.height = floor(image.size.height / (image.size.width / self.width))
            } else {
                var height = image.size.height / image.size.width * self.width
                if (height < 1)  {
                    height = self.height
                }
                height = floor(height)
                imageContainerView.height = height
                imageContainerView.centerY = self.height / 2
            }
            if (imageContainerView.height > self.height && imageContainerView.height - self.height <= 1) {
                imageContainerView.height = self.height
            }
            scrollView.contentSize = CGSizeMake(self.width, max(imageContainerView.height, self.height))
            scrollView.scrollRectToVisible(self.bounds, animated:false)
            scrollView.alwaysBounceVertical = imageContainerView.height <= self.height ? false : true
            imageView.frame = imageContainerView.bounds
        }
    }
}

extension PreviewCell:UIScrollViewDelegate {
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageContainerView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        let offsetX = (scrollView.width > scrollView.contentSize.width) ? (scrollView.width - scrollView.contentSize.width) * 0.5 : 0.0
        let offsetY = (scrollView.height > scrollView.contentSize.height) ? (scrollView.height - scrollView.contentSize.height) * 0.5 : 0.0
        self.imageContainerView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX, scrollView.contentSize.height * 0.5 + offsetY)
    }
}
