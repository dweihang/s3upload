//
//  DHAlbumPickerController.swift
//  XFBConsumer
//
//  Created by hy110831 on 6/28/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import UIKit
import Photos

class DHAlbumPickerController: UIViewController {
    
    let SELECTED_TYPES:[PHAssetCollectionSubtype:Bool] = {
        var types:[PHAssetCollectionSubtype:Bool] = [.SmartAlbumUserLibrary:true, .SmartAlbumRecentlyAdded:true,.SmartAlbumFavorites:true, .SmartAlbumBursts:true]
        if #available(iOS 9.0, *) {
            types[.SmartAlbumScreenshots] = true
            types[.SmartAlbumSelfPortraits] = true
        }
        return types
    }()
    
    private(set) lazy var albums:[(PHAssetCollection,PHAsset,Int)] = []
//    private(set) lazy var albumsFetchResult = [PHAssetCollection:PHFetchResult]()
    
    private var tableView:UITableView
    private var isLoaded:Bool
    private var imageManager:PHCachingImageManager
    
    init(){
        isLoaded = false
        tableView = UITableView(frame: CGRectZero, style: .Grouped)
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.separatorStyle = .None
        imageManager = PHCachingImageManager()
        imageManager.stopCachingImagesForAllAssets()
        super.init(nibName:nil, bundle:nil)
        self.navigationItem.title = "albums".localized
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "cancel".localized, style: .Plain, target: self, action: #selector(dismiss))
        tableView.registerClass(AlbumCell.self, forCellReuseIdentifier: "AlbumCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    deinit {
        albums.removeAll()
        print(#function, "\(self)")
//        albumsFetchResult.removeAll()
    }
    
    func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        clearPickDataSource()
    }
    
    override func viewWillLayoutSubviews() {
        tableView.frame = self.view.bounds
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        loadAlbums()
    }
    
    func clearPickDataSource() {
        if let naviVC = self.navigationController as? DHImagePickerController {
            naviVC.dataSource.editAssets.removeAll()
            naviVC.dataSource.commit()
        }
    }
    
    func getPickDataSource()->DHImagePickerDataSource? {
        if let naviVC = self.navigationController as? DHImagePickerController {
            return naviVC.dataSource
        }
        return nil
    }
    
    
    func loadAlbums() {
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "mediaType == %ld",PHAssetMediaType.Image.rawValue)

        let querySmartAlbums = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .AlbumRegular, options: nil)
        
        querySmartAlbums.enumerateObjectsUsingBlock { [unowned self, unowned querySmartAlbums] (collection, index, stop) in
            
            if let type = (collection as? PHAssetCollection)?.assetCollectionSubtype {
                if self.SELECTED_TYPES[type] == true {
                    let fetchResult = PHAsset.fetchAssetsInAssetCollection(collection as! PHAssetCollection, options: options)
                    if fetchResult.count != 0 {
                        self.albums.append((collection as! PHAssetCollection,fetchResult.lastObject as! PHAsset,fetchResult.count))
//                        albumCoverPhotoAssets.append(fetchResult.lastObject as! PHAsset
//                        self.albumsFetchResult[collection as! PHAssetCollection] = fetchResult
                        
                    }
                }
            }
            if index == querySmartAlbums.count - 1{
                stop.memory = true
            }
        }
        
        let queryUserAlbums = PHCollectionList.fetchTopLevelUserCollectionsWithOptions(nil)
        
        queryUserAlbums.enumerateObjectsUsingBlock { [unowned self, unowned queryUserAlbums] (collection, index, stop) in
            let fetchResult = PHAsset.fetchAssetsInAssetCollection(collection as! PHAssetCollection, options: options)
            if fetchResult.count != 0 {
                self.albums.append((collection as! PHAssetCollection,fetchResult.lastObject as! PHAsset, fetchResult.count))
                
//                self.albumsFetchResult[collection as! PHAssetCollection] = fetchResult
            }
            if index == queryUserAlbums.count - 1{
                stop.memory = true
            }
        }
        isLoaded = true
        self.tableView.reloadData()
    }
    
}

extension DHAlbumPickerController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let collection = albums[indexPath.row].0
//        let fetchResult = albumsFetchResult[collection]!
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "mediaType == %ld",PHAssetMediaType.Image.rawValue)
        
        let fetchResult = PHAsset.fetchAssetsInAssetCollection(collection , options: options)
        
        if let pickerDataSource = getPickDataSource() {
            let photoPickerVC = DHPhotoPickerController(dataSource: pickerDataSource, assetCollection: collection, assetsFetchResult: fetchResult)
            self.navigationController?.pushViewController(photoPickerVC, animated: true)
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let collection = albums[indexPath.row].0
        let asset = albums[indexPath.row].1
//        let asset = albumsFetchResult[collection]!.lastObject as! PHAsset
        let cell = tableView.dequeueReusableCellWithIdentifier("AlbumCell") as! AlbumCell
        cell.fetchCoverImageForCollection(asset, imageManager: self.imageManager)
        cell.titleLabel.attributedText = NSAttributedString(string: collection.localizedTitle!, attributes: cell.textStyle())
        cell.countLabel.attributedText = NSAttributedString(string: String(format: "(%d)", albums[indexPath.row].2
), attributes: cell.textStyle())
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
}

class AlbumCell: UITableViewCell {
    
    var coverImageView: UIImageView!
    var titleLabel:UILabel!
    var countLabel:UILabel!
    
    var nextImageView:UIImageView!
    var separator: UIView!
    
    let PADDING_IMG:CGFloat = 2
    let CELL_HEIGHT:CGFloat = 80
    
    var once_token = false
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .None
        self.titleLabel = UILabel()
        self.countLabel = UILabel()
        self.separator = UIView()
        self.nextImageView = UIImageView()
        self.coverImageView = UIImageView()
        self.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(countLabel)
        self.contentView.addSubview(separator)
        self.contentView.addSubview(nextImageView)
        self.contentView.addSubview(coverImageView)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func textStyle()-> [String:AnyObject] {
        let font = StyleHelper.getAppFontWithSize(17)
        return [NSFontAttributeName:font, NSForegroundColorAttributeName: UIColor.blackColor()]
    }
    
    func fetchCoverImageForCollection(asset:PHAsset, imageManager:PHImageManager) {
        let opts = PHImageRequestOptions()
        opts.networkAccessAllowed = true
        
        imageManager.requestImageForAsset(asset, targetSize: CGSizeMake(CELL_HEIGHT - 2 * PADDING_IMG, CELL_HEIGHT - 2 * PADDING_IMG), contentMode: PHImageContentMode.AspectFit, options: opts) { [unowned self] (image, info) in
            self.coverImageView.image = image
        }
    }
    
    func setupView() {
        let pixelHeight:CGFloat = 1 / UIScreen.mainScreen().scale
        separator.frame = CGRectMake(0, 0, self.contentView.width, pixelHeight)
        separator.backgroundColor = UIColor(red: 200/255, green:199/255, blue:204/255, alpha:1)
        
        titleLabel.frame = CGRectMake(0, 0, 100, 30)
        
        countLabel.frame = CGRectMake(0, 0, 100, 30)
        
        let image = UIImage(named: "dhpicker_right_arrow.png")!
        nextImageView.frame = CGRectMake(0, 0, image.size.width, image.size.height)
        nextImageView.image = image
        
    }
    
    override func layoutSubviews() {
//        if !once_token {
//            setupView()
//            once_token = true
//        }
        
                coverImageView.frame = CGRectMake(0, PADDING_IMG, self.contentView.height - 2 * PADDING_IMG, self.contentView.height - 2 * PADDING_IMG)
        coverImageView.centerY = self.contentView.centerY
        let pixelHeight:CGFloat = 1 / UIScreen.mainScreen().scale
        separator.y = self.contentView.height - pixelHeight
        
        titleLabel.sizeToFit()
        titleLabel.centerY = coverImageView.centerY
        titleLabel.x = coverImageView.right + 5
        
        countLabel.sizeToFit()
        countLabel.centerY = titleLabel.centerY
        countLabel.x = titleLabel.right + 5
        
        nextImageView.right = self.contentView.width - 13
        nextImageView.centerY = self.contentView.height / 2
    }
    
    override func prepareForReuse() {
        self.imageView?.image = nil
        self.titleLabel.text = nil
        self.countLabel.text = nil
        super.prepareForReuse()
    }
}
