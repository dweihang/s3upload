////
////  StyleHelper.swift
////  s3upload
////
////  Created by hy110831 on 7/20/16.
////  Copyright © 2016 hy110831. All rights reserved.
////
//

import UIKit

class StyleHelper {
//
//    static let scale = UIScreen.mainScreen().scale
//    
    // MARK: APP Theme
    static func getAppThemeColor()-> UIColor {
        return UIColor(red: 239/255, green: 97/255, blue: 86/255, alpha: 1)
    }
    
    static func getAppThemeSecondaryColor()-> UIColor {
        return UIColor(red: 44/255, green: 46/255, blue: 51/255, alpha: 1)
    }
//
    static func getAppFontWithSize(size:CGFloat)->UIFont {
        return UIFont.systemFontOfSize(size)
    }

    static func getAppBoldFontWithSize(size:CGFloat)->UIFont {
        return UIFont.boldSystemFontOfSize(size)
    }
//
//    
//    // MARK: APP TableView
    static func getTableViewBackgroundColor()->UIColor {
        return UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
    }
    
//
//    static func getTableViewMainLabelTextColor()->UIColor {
//        return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//    }
//    
//    static func getTableViewDetailLabelTextColor()->UIColor {
//        return UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
//    }
//    
    static func getSeparatorColor()->UIColor {
        return UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
    }
//
//    static func addTableViewSeparatorToView(superview:UIView)->UIView {
//        let separator = UIView()
//        separator.backgroundColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
//        superview.addSubview(separator)
//        separator.snp_makeConstraints { [unowned superview](make) in
//            make.bottom.equalTo(superview.snp_bottom)
//            make.width.equalTo(superview)
//            make.height.equalTo(1 / scale)
//        }
//        return separator
//    }
//    
//    static func createSeparatorView()-> UIView {
//        let separator = UIView()
//        separator.backgroundColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
//        return separator
//    }
//    
//    static func addGradientWithTopColor(topColor:UIColor, bottomColor:UIColor, view:UIView) {
//        let gradient = CAGradientLayer()
//        gradient.frame = view.bounds
//        gradient.colors = [topColor.CGColor, bottomColor.CGColor];
//        view.layer.insertSublayer(gradient, atIndex: 0);
//    }
//    
    // MARK: ActionSheet
    static func getActionSheetTitleColor()->UIColor {
        return UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
    }
    
    static func getActionSheetActionTextColor()->UIColor {
        return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    }

    static func getActionSheetBackgroundColor()->UIColor {
        return UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
    }
//
//    // MARK: Top Hint View
//    static func showTopHintErrorWithTitle(title:String, detailMessage:String, duration:CGFloat = 1.5) {
//        TWMessageBarManager.sharedInstance().showMessageWithTitle(title, description: detailMessage, type: TWMessageBarMessageType.Error, duration: duration)
//    }
//    
//    static func showTopHintInfoWithTitle(title:String, detailMessage:String, duration:CGFloat = 1.5) {
//        TWMessageBarManager.sharedInstance().showMessageWithTitle(title, description: detailMessage, type: TWMessageBarMessageType.Info, duration: duration)
//    }
//    
//    static func showTopHintSuccessWithTitle(title:String, detailMessage:String, duration:CGFloat = 1.5) {
//        TWMessageBarManager.sharedInstance().showMessageWithTitle(title, description: detailMessage, type: TWMessageBarMessageType.Success, duration: duration)
//    }
//    
//    // MARK: Alert
//    static func showAlertWithTitle(title:String, doneText:String, doneCallback: ((CustomIOSAlertView)->(Void))?) {
//        showAlertWithTitle(title, titleBoldFont: true, doneText: doneText, doneCallback: doneCallback)
//    }
//    
//    static func showAlertWithTitle(title:String, contentText:String, doneText:String, doneCallback: ((CustomIOSAlertView)->(Void))?)->CustomIOSAlertView {
//        let customAlertView = CustomIOSAlertView()
//        let alertView = AlertView.Builder().title(title).boldTitle(true).content(contentText).singleButtonText(doneText).singleButtonColor(StyleHelper.getAppThemeColor()).setCustomLayout({ (alertView) in
//            
//            alertView.contentLabel?.numberOfLines = 0
//            
//            let paragraphStyles = NSMutableParagraphStyle()
//            paragraphStyles.alignment = NSTextAlignment.Justified
//            paragraphStyles.lineBreakMode = .ByWordWrapping
//            paragraphStyles.lineSpacing = 2
//            paragraphStyles.paragraphSpacing = 5
//            let textAttrs = [NSForegroundColorAttributeName:UIColor(red: 177/255, green: 175/255, blue: 175/255, alpha: 1), NSParagraphStyleAttributeName:paragraphStyles,NSFontAttributeName:StyleHelper.getAlertViewContentFont(), NSBaselineOffsetAttributeName: NSNumber(float: 0)]
//            let text = contentText
//            alertView.contentLabel?.text = nil
//            alertView.contentLabel?.attributedText = NSAttributedString(string: text, attributes: textAttrs)
//            
//            let titleTop = 15 * ScaleFactor.VERTICAL
//            let titleSize = alertView.titleLabel!.sizeThatFits(CGSizeMake(400, 400))
//            
//            alertView.titleLabel!.snp_remakeConstraints(closure: { (make) in
//                make.centerX.equalTo(alertView.titleLabel!.superview!)
//                make.size.equalTo(titleSize)
//                make.top.equalTo(alertView.titleLabel!.superview!).offset(titleTop)
//            })
//            
//            
//            let contentTop = 15 * ScaleFactor.VERTICAL
//            let contentBottom = 15 * ScaleFactor.VERTICAL
//            let contentSize = alertView.contentLabel!.sizeThatFits(CGSizeMake(AlertView.WIDTH - 2 * 30 * ScaleFactor.HORIZONTAL, CGFloat.max))
//            
//            alertView.contentLabel?.snp_remakeConstraints(closure: { (make) in
//                make.centerX.equalTo(alertView.titleLabel!.superview!)
//                make.width.equalTo(contentSize.width)
//                make.height.equalTo(contentSize.height)
//                make.top.equalTo(alertView.titleLabel!.snp_bottom).offset(contentTop)
//            })
//            
//            let totalHeight =  titleTop + titleSize.height + contentTop + contentSize.height + contentBottom + AlertView.HEIGHT_BUTTON + AlertView.BOTTOM_BUTTON
//            
//            alertView.height = totalHeight
//        }).build()
//        if let callback = doneCallback {
//            alertView.singleBtn?.addTarget(.TouchUpInside, action: {
//                callback(customAlertView)
//            })
//        }
//        customAlertView.containerView = alertView
//        customAlertView.showAnimated(true)
//        return customAlertView
//    }
//    
//    static func showAlertWithTitle(title:String, doneText:String, cancelText:String, doneCallback:((CustomIOSAlertView)->(Void))?) {
//        let customAlertView = CustomIOSAlertView()
//        let alertView = AlertView.Builder().title(title).boldTitle(true).rightButtonText(doneText).rightButtonColor(StyleHelper.getAppThemeColor()).leftButtonText("cancel".localized).leftButtonColor(UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)).setCustomLayout({ (alertView) in
//            
//            alertView.titleLabel?.numberOfLines = 0
//            
//            let computedSize = alertView.titleLabel!.sizeThatFits(CGSizeMake(AlertView.WIDTH - 2 * 30 * ScaleFactor.HORIZONTAL, CGFloat.max))
//            let height = computedSize.height
//            let width = computedSize.width
//            
//            let totalHeight =  height + 30 * ScaleFactor.VERTICAL + AlertView.HEIGHT_BUTTON + AlertView.BOTTOM_BUTTON
//            var titleTop = 30 * ScaleFactor.VERTICAL
//            if totalHeight > AlertView.HEIGHT {
//                alertView.frame = CGRectMake(0, 0, AlertView.WIDTH, totalHeight)
//                titleTop = 20 * ScaleFactor.VERTICAL
//            }
//            alertView.titleLabel?.snp_remakeConstraints(closure: { (make) in
//                make.centerX.equalTo(alertView.titleLabel!.superview!)
//                make.width.equalTo(width)
//                make.height.equalTo(height)
//                make.top.equalTo(titleTop)
//            })
//        }).build()
//        if let callback = doneCallback {
//            alertView.rightBtn?.addTarget(.TouchUpInside, action: {
//                callback(customAlertView)
//            })
//        }
//        alertView.leftBtn?.addTarget(.TouchUpInside, action: {
//            customAlertView.close()
//        })
//        customAlertView.containerView = alertView
//        customAlertView.showAnimated(true)
//    }
//    
//    static func showAlertWithTitle(title:String, titleBoldFont:Bool, doneText:String, doneCallback: ((CustomIOSAlertView)->(Void))?) {
//        let customAlertView = CustomIOSAlertView()
//        let alertView = AlertView.Builder().title(title).boldTitle(titleBoldFont).singleButtonText(doneText).singleButtonColor(StyleHelper.getAppThemeColor()).setCustomLayout({ (alertView) in
//            alertView.titleLabel?.numberOfLines = 0
//            
//            let computedSize = alertView.titleLabel!.sizeThatFits(CGSizeMake(AlertView.WIDTH - 2 * 30 * ScaleFactor.HORIZONTAL, CGFloat.max))
//            let height = computedSize.height
//            let width = computedSize.width
//            
//            let totalHeight =  height + 30 * ScaleFactor.VERTICAL + AlertView.HEIGHT_BUTTON + AlertView.BOTTOM_BUTTON
//            var titleTop = 30 * ScaleFactor.VERTICAL
//            if totalHeight > AlertView.HEIGHT {
//                alertView.frame = CGRectMake(0, 0, AlertView.WIDTH, totalHeight)
//                titleTop = 20 * ScaleFactor.VERTICAL
//            }
//            alertView.titleLabel?.snp_remakeConstraints(closure: { (make) in
//                //                make.left.equalTo(alertView.snp_left).offset(30 * ScaleFactor.HORIZONTAL)
//                //                make.right.equalTo(alertView.snp_right).offset(-30 * ScaleFactor.HORIZONTAL)
//                make.centerX.equalTo(alertView.titleLabel!.superview!)
//                make.width.equalTo(width)
//                make.height.equalTo(height)
//                make.top.equalTo(titleTop)
//            })
//        }).build()
//        if let callback = doneCallback {
//            alertView.singleBtn?.addTarget(.TouchUpInside, action: {
//                callback(customAlertView)
//            })
//        }
//        customAlertView.containerView = alertView
//        customAlertView.showAnimated(true)
//    }
//    
//    static func getAlertViewBackgroundColor()->UIColor {
//        return UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
//    }
//    
//    static func getAlertViewTitleFont()->UIFont {
//        return UIFont.systemFontOfSize(18)
//    }
//    
//    static func getAlertViewTitleBoldFont()->UIFont {
//        return UIFont.boldSystemFontOfSize(18)
//    }
//    
//    static func getAlertViewContentFont()->UIFont {
//        return UIFont.systemFontOfSize(15)
//    }
//    
//    static func getAlertViewTitleColor()->UIColor {
//        return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//    }
//    
//    static func getAlertViewContentColor()->UIColor {
//        return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//    }
//    
//    static func getAlertViewButtonFont()->UIFont {
//        return UIFont.systemFontOfSize(18)
//    }
//    
//    static func getAlertViewButtonBackground()->UIColor {
//        return UIColor.whiteColor()
//    }
//    
//    static func getAlertViewButtonColor()->UIColor {
//        return UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
//    }
//    
//    static func getAlertViewButtonBorderColor()->UIColor {
//        return UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1)
//    }
//    
//    
//    // MARK : Phone alert
//    static func showPhoneAlertWithTitle(title:String, phoneNum:String) {
//        let alertView = CustomIOSAlertView()
//        let alertContent = AlertView.Builder().leftButtonText("cancel".localized).rightButtonText("拨号").rightButtonColor(StyleHelper.getAppThemeColor()).title(title).setCustomLayout { (rootView) in
//            let label = UILabel()
//            label.font = StyleHelper.getAlertViewTitleFont()
//            label.textAlignment = .Center
//            label.textColor = UIColor.blackColor()
//            label.backgroundColor = UIColor.whiteColor()
//            label.layer.cornerRadius = 5
//            label.layer.borderColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 1).CGColor
//            label.layer.borderWidth = 1
//            var text:NSMutableString? = nil
//            if phoneNum.length == 11 {
//                text = NSMutableString(string: phoneNum)
//                text!.insertString("-", atIndex: 8)
//                text!.insertString("-", atIndex: 4)
//                text!.insertString("-", atIndex: 2)
//            }
//            label.tag = 8080
//            rootView.addSubview(label)
//            if let txt = text {
//                label.text = txt as String
//            }
//            label.snp_makeConstraints(closure: { (make) in
//                make.left.equalTo(rootView.snp_left).offset(14 * ScaleFactor.HORIZONTAL)
//                make.right.equalTo(rootView.snp_right).offset(-14 * ScaleFactor.HORIZONTAL)
//                make.centerX.equalTo(rootView.snp_centerX)
//                make.centerY.equalTo(rootView.snp_centerY).offset(-8)
//                make.height.equalTo(40)
//            })
//            }.layoutTitleTopMargin(13).build()
//        alertContent.leftBtn?.addTarget(.TouchUpInside, action: { [weak alertView] in
//            alertView?.close()
//            })
//        alertContent.rightBtn?.addTarget(.TouchUpInside, action: { [weak alertView] in
//            if let label = alertView?.viewWithTag(8080) as? UILabel {
//                if let text = label.text {
//                    if let phoneUrl = NSURL(string: String(format: "telprompt:%@", text)) {
//                        if UIApplication.sharedApplication().canOpenURL(phoneUrl) {
//                            UIApplication.sharedApplication().openURL(phoneUrl)
//                            return
//                        }
//                    }
//                }
//                let unableCallAlert = CustomIOSAlertView()
//                let unableCallAlertContentView = AlertView.Builder().singleButtonText("我知道了").singleButtonColor(StyleHelper.getAppThemeColor()).title("亲，这个设备不支持打电话哦").build()
//                unableCallAlertContentView.singleBtn?.addTarget(.TouchUpInside, action: {
//                    unableCallAlert.close()
//                })
//                
//                unableCallAlert.containerView = unableCallAlertContentView
//                alertView?.close()
//                
//                unableCallAlert.showAnimated(true)
//            }
//            })
//        alertView.containerView = alertContent
//        if phoneNum.length == 11 {
//            alertView.showAnimated(true)
//        }
//    }
//    
//    // MARK: Button
//    static func createAppSubmitBtnWithTitle(title:String)->UIButton {
//        let btn = UIButton()
//        btn.layer.cornerRadius = 22 * ScaleFactor.VERTICAL
//        btn.backgroundColor = StyleHelper.getAppThemeColor()
//        btn.setTitleColor(UIColor.whiteColor(), forState: .Normal)
//        btn.setTitle(title, forState: .Normal)
//        return btn
//    }
//    
//    
//    // MARK: Show Photo Browser with urls
//    static func showPhotoBrowsersWithUrls(urls:[String], currentViewController viewcontroller:UIViewController) {
//        var photos = [IDMPhoto]()
//        for imageUrl in urls {
//            let photo = IDMPhoto(URL: NSURL(string:imageUrl))
//            photos.append(photo)
//        }
//        
//        let browser = IDMPhotoBrowser(photos: photos)
//        browser.displayActionButton = false
//        browser.displayArrowButton = false
//        browser.displayCounterLabel = false
//        browser.displayDoneButton = false
//        browser.disableVerticalSwipe = true
//        viewcontroller.presentViewController(browser, animated: true, completion: nil)
//    }
//    
//    // MARK: Invoke call
//    static func invokeCallForNumber(phone:String){
//        if let phoneUrl = NSURL(string: String(format: "telprompt:%@", phone)) {
//            if UIApplication.sharedApplication().canOpenURL(phoneUrl) {
//                UIApplication.sharedApplication().openURL(phoneUrl)
//                return
//            }
//        }
//    }
//    
//    // MARK: Currency Number formatter
//    static var currencyFormatter:NSNumberFormatter = {
//        let numberFormatter = NSNumberFormatter()
//        numberFormatter.numberStyle = .CurrencyStyle
//        numberFormatter.currencySymbol = ""
//        return numberFormatter
//    }()
}