//
//  EPExtensions.swift
//  EPCalendar
//
//  Created by Prabaharan Elangovan on 29/11/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import Foundation
import UIKit

// DateExt里面有重复定义
//internal func ==(lhs: NSDate, rhs: NSDate) -> Bool {
//    return lhs.compare(rhs) == NSComparisonResult.OrderedSame
//}
//
//internal func <(lhs: NSDate, rhs: NSDate) -> Bool {
//    return lhs.compare(rhs) == NSComparisonResult.OrderedAscending
//}
//
//internal func >(lhs: NSDate, rhs: NSDate) -> Bool {
//    return rhs.compare(lhs) == NSComparisonResult.OrderedAscending
//}

//MARK: NSDate Extensions

extension NSDate {
    
    func sharedCalendar(){
        
    }
    
    func firstDayOfMonth () -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components([.Year, .Month, .Day ], fromDate: self)
        dateComponent.day = 1
        return calendar.dateFromComponents(dateComponent)!
    }
    
    func daysOfMonth() -> Int {
        var days = 30
        let month = self.month()
        let year = self.year()
        switch month {
        case 1, 3, 5, 7, 8, 10, 12:
            days = 31
        case 4, 6, 9, 11:
            days = 30
        case 2:
            // 能被4整除且不能被100整除
            // 能被100整除且能被400整除
            if (year%4 == 8 && year%100 != 0) || (year%100 == 0 && year%400 == 0) {
                days = 29
            } else {
                days = 28
            }
        default:
            days = 30
        }
        return days
    }
    
    func lastDayOfMonth() -> NSDate {
        var days = 30
        let month = self.month()
        let year = self.year()
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components([.Year, .Month, .Day ], fromDate: self)
        switch month {
        case 1, 3, 5, 7, 8, 10, 12:
            days = 31
        case 4, 6, 9, 11:
            days = 30
        case 2:
            // 能被4整除且不能被100整除
            // 能被100整除且能被400整除
            if (year%4 == 8 && year%100 != 0) || (year%100 == 0 && year%400 == 0) {
                days = 29
            } else {
                days = 28
            }
        default:
            days = 30
        }
        dateComponent.day = days
        return calendar.dateFromComponents(dateComponent)!
    }
    
    func dayOfMonth(day: Int) -> NSDate? {
        let maxDays = self.daysOfMonth()
        guard day > 0 && day <= maxDays else {
            let domain = "参数错误[1, " + String(maxDays) + "]"
            print(domain)
            return nil
        }
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components([.Year, .Month, .Day ], fromDate: self)
        dateComponent.day = Int(day)
        return calendar.dateFromComponents(dateComponent)!
    }
    
    convenience init(year : Int, month : Int, day : Int) {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = NSDateComponents()
        dateComponent.year = year
        dateComponent.month = month
        dateComponent.day = day
        self.init(timeInterval:0, sinceDate:calendar.dateFromComponents(dateComponent)!)
    }
    
    func dateByAddingMonths(months : Int ) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = NSDateComponents()
        dateComponent.month = months
        return calendar.dateByAddingComponents(dateComponent, toDate: self, options: NSCalendarOptions.MatchNextTime)!
    }
    
    func dateByAddingDays(days : Int ) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = NSDateComponents()
        dateComponent.day = days
        return calendar.dateByAddingComponents(dateComponent, toDate: self, options: NSCalendarOptions.MatchNextTime)!
    }
    
    func hour() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Hour, fromDate: self)
        return dateComponent.hour
    }
    
    func second() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Second, fromDate: self)
        return dateComponent.second
    }
    
    func minute() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Minute, fromDate: self)
        return dateComponent.minute
    }
    
    func day() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Day, fromDate: self)
        return dateComponent.day
    }
    
    func weekday() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Weekday, fromDate: self)
        return dateComponent.weekday
    }
    
    func weekdayStr() -> String {
        return NSCalendar.currentCalendar().weekdaySymbols[self.weekday()-1]
    }
    
    func month() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Month, fromDate: self)
        return dateComponent.month
    }
    
    func monthStr() -> String {
        return NSCalendar.currentCalendar().monthSymbols[self.month()-1]
    }
    
    func year() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components(.Year, fromDate: self)
        return dateComponent.year
    }
    
    func numberOfDaysInMonth() -> Int {
        let calendar = NSCalendar.currentCalendar()
        let days = calendar.rangeOfUnit(NSCalendarUnit.Day, inUnit: NSCalendarUnit.Month, forDate: self)
        return days.length
    }
    
    func dateByIgnoringTime() -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponent = calendar.components([.Year, .Month, .Day ], fromDate: self)
        return calendar.dateFromComponents(dateComponent)!
    }
    
    func monthNameFull() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        return dateFormatter.stringFromDate(self)
    }
    
    func isSunday() -> Bool
    {
        return (self.getWeekday() == 1)
    }
    
    func isMonday() -> Bool
    {
        return (self.getWeekday() == 2)
    }
    
    func isTuesday() -> Bool
    {
        return (self.getWeekday() == 3)
    }
    
    func isWednesday() -> Bool
    {
        return (self.getWeekday() == 4)
    }
    
    func isThursday() -> Bool
    {
        return (self.getWeekday() == 5)
    }
    
    func isFriday() -> Bool
    {
        return (self.getWeekday() == 6)
    }
    
    func isSaturday() -> Bool
    {
        return (self.getWeekday() == 7)
    }
    
    func getWeekday() -> Int {
        let calendar = NSCalendar.currentCalendar()
        return calendar.components( .Weekday, fromDate: self).weekday
    }
    
    func isToday() -> Bool {
        return self.isDateSameDay(NSDate())
    }
    
    func isDateSameDay(date: NSDate) -> Bool {

         return (self.day() == date.day()) && (self.month() == date.month() && (self.year() == date.year()))

    }
}
