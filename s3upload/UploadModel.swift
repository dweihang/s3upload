//
//  UploadModel.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import SwiftyJSON

extension JSON {
    func parseServerDate()->NSDate? {
        if let dateStr = self.string {
            return dateFormatter.dateFromString(dateStr)
        }
        return nil
    }
}

var dateFormatter: NSDateFormatter {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyyMMdd'T'HHmmss'Z'"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
    return formatter
}

class UploadModel: NSObject {
    var action: String?
    var method: String?
    var enctype: String?
    
    var acl: String?
    var keyPath: String?
    var X_Amz_Credential:String?
    var X_Amz_Algorithm:String?
    var X_Amz_Date:String?
    var Policy:String?
    var X_Amz_Signature:String?
    
    lazy var fileName = (NSUUID().UUIDString) + ".jpg"
    
    func isValid()-> Bool {
        if self.method == nil {
            return false
        }
        if self.action == nil {
            return false
        }
        if self.X_Amz_Credential == nil {
            return false
        }
        if self.X_Amz_Algorithm == nil {
            return false
        }
        if self.X_Amz_Signature == nil {
            return false
        }
        if self.Policy == nil {
            return false
        }
        if self.X_Amz_Date == nil {
            return false
        }
        if self.acl == nil {
            return false
        }
        if self.keyPath == nil {
            return false
        }
        return true
    }
    
    func toDictionary()->[String:AnyObject]? {
        if !self.isValid() {
            return nil
        }
        return ["acl": self.acl!,
                "key": keyPath! + self.fileName,
         "X-Amz-Credential": self.X_Amz_Credential!,
         "X-Amz-Algorithm": self.X_Amz_Algorithm!,
         "X-Amz-Date": self.X_Amz_Date!,
         "Policy": self.Policy!,
         "X-Amz-Signature": self.X_Amz_Signature!,
        
        ]
    }
    
    class func populateFromJSON(json:JSON)-> UploadModel? {
        let model = UploadModel()
        let data = json["data"]
        model.action = data["form"]["action"].nonEmptyString()
        model.method = data["form"]["method"].nonEmptyString()
        model.enctype = data["form"]["enctype"].nonEmptyString()
        
        model.acl = data["inputs"]["acl"].nonEmptyString()
        model.keyPath = data["key"].nonEmptyString()
        model.X_Amz_Credential = data["inputs"]["X-Amz-Credential"].nonEmptyString()
        model.X_Amz_Algorithm = data["inputs"]["X-Amz-Algorithm"].nonEmptyString()
        model.X_Amz_Date = data["inputs"]["X-Amz-Date"].nonEmptyString()
        model.Policy = data["inputs"]["Policy"].nonEmptyString()
        model.X_Amz_Signature = data["inputs"]["X-Amz-Signature"].nonEmptyString()
        if model.isValid() {
            return model
        }
        return nil
    }
}