//
//  UITabBar+CustomBadge.swift
//  XFBConsumer
//
//  Created by hy110831 on 5/31/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import UIKit

extension UITabBar {
    
    func showBadgeOnItemIndex(index: Int) {
        //移除之前的小红点
        let TabbarItemNums:CGFloat = 4
        self.removeBadgeOnItemIndex(index)
        //新建小红点
        let badgeView: UIView = UIView()
        badgeView.tag = 888 + index
        badgeView.layer.cornerRadius = 5
        //圆形
        badgeView.backgroundColor = UIColor.redColor()
        //颜色：红色
        let tabFrame: CGRect = self.frame
        //确定小红点的位置
        let percentX: CGFloat = (CGFloat(index) + 0.6) / TabbarItemNums
        let x: CGFloat = CGFloat(ceilf(Float(percentX * tabFrame.size.width)))
        let y: CGFloat = CGFloat(ceilf(Float(0.1 * tabFrame.size.height)))
        badgeView.frame = CGRectMake(x, y, 10, 10)
        //圆形大小为10
        self.addSubview(badgeView)
    }
    
    //移除小红点
    
    func removeBadgeOnItemIndex(index: Int) {
        //按照tag值进行移除
        for subView: UIView in self.subviews {
            if subView.tag == 888 + index {
                subView.removeFromSuperview()
            }
        }
    }
}