//
//  ApiHelper.swift
//  s3upload
//
//  Created by hy110831 on 7/20/16.
//  Copyright © 2016 hy110831. All rights reserved.
//

import Foundation
import Bolts
import SwiftyJSON
import Photos

protocol ApiError {
    func getApiErrorMessage()->String
    func getApiErrorCode()->ApiErrorCode
}

extension NSError:ApiError {
    func getApiErrorMessage()->String {
        return (self.userInfo["_ApiErrorMessage"] as! String)
    }
    
    func getApiErrorCode()->ApiErrorCode {
        return ApiErrorCode(rawValue:self.code)!
    }
}

enum ApiErrorCode:Int {
    case Internal = 1000
    case EmptyResponse = 1001
    case ProtocolError = 1002
    
    case ParseRequestJSONError = 2000
    case ParseResponseJSONError = 2001
    case ExpectedResponseHeadersMissing = 2002
    
    case RequestDataInvalid = 3000
    
    case MultiLogin = 4000
    case LoginCredentialInvalid = 4001
    
    func defaultErrorMessage()->String {
        switch self {
        case .Internal:
            return "内部错误";
        case .EmptyResponse:
            return "服务器返回为空"
        case .ProtocolError:
            return "服务器返回协议错误或不支持该HTTP方法"
        case .ParseRequestJSONError:
            return "解析请求数据失败"
        case .ParseResponseJSONError:
            return "解析服务器响应数据失败"
        case .ExpectedResponseHeadersMissing:
            return "服务器没有返回相应header"
        case .MultiLogin:
            return "您的账号已在其他设备登陆"
        case .LoginCredentialInvalid:
            return "登录失败，请检查你的登录号码或密码"
        default:
            return "业务错误"
        }
    }
}

class ApiHelper {
    
    static let SERVER_URL = "http://test-app.cozystay.com/api/v1"
    static let ERROR_DOMAIN = "COZYSTAY"
    static let USER_AGENT = "COZYSTAY 0.0.1, Model:" + DeviceModel.MODEL
    
    static let REQUEST_TIMEOUT:NSTimeInterval = 60
    
    
    func fetchUploadImageFromAsset(photoAsset:PHAsset)-> BFTask {
        let taskSource = BFTaskCompletionSource()
        let options = PHImageRequestOptions()
        options.networkAccessAllowed = true
        options.resizeMode = PHImageRequestOptionsResizeMode.Exact
        options.deliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat
        let aspectRatio:CGFloat = CGFloat(photoAsset.pixelWidth) / CGFloat(photoAsset.pixelHeight)
        var targetWidth:CGFloat = CGFloat(photoAsset.pixelWidth) < 1028.0 ? CGFloat(photoAsset.pixelWidth) : 1028.0
        var targetHeight = targetWidth / aspectRatio
        if targetHeight > 1080 {
            targetHeight = 1080
            targetWidth = targetHeight * aspectRatio
        }
        PHImageManager.defaultManager().requestImageForAsset(photoAsset, targetSize: CGSizeMake(targetWidth, targetHeight), contentMode: PHImageContentMode.AspectFill, options: options, resultHandler: { (resultImg, _) in
            autoreleasepool({
                if resultImg != nil {
                    let resultData = UIImageJPEGRepresentation(resultImg!, 0.6)!
                    taskSource.trySetResult(resultImg)
                    print("Image width: \(resultImg!.size.width), height: \(resultImg!.size.height)")
                    print(String(format:"File size is : %.1f KB", Float(resultData.length) / 1024.0))
                } else {
                    taskSource.setError(NSError(domain: "PhotoFramework", code: -1, userInfo: ["reason":"failed to fetch photo"]))
                }
            })
            
        })
        return taskSource.task
    }
    
    func S3batchUploadImagesWithDatasource(dataSource:DHImagePickerDataSource)-> BFTask {
        return BFTask(fromExecutor: BFExecutor(dispatchQueue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)), withBlock: { [unowned self] in
            var tasks:[BFTask] = []
            for asset in dataSource.assets {
                if let photoAsset = asset.photoAsset {
                    
                    tasks.append(self.fetchUploadImageFromAsset(photoAsset).continueWithSuccessBlock({ [unowned self](task) -> AnyObject? in
                        let img = task.result as! UIImage
                        return self.S3uploadImage(img)
                    }))
                }
                else if let image = asset.image {
                    tasks.append(self.S3uploadImage(image))
                }
            }
            return BFTask(forCompletionOfAllTasks: tasks)
        })
    }
    
    func S3uploadImage(image:UIImage)-> BFTask {
        let url = ApiHelper.prepareApiUrlForPath("/util/s3_post")
        return self.baseHTTPGetRequest(url, params: nil, requestHeaders: nil).continueWithBlock({ (task) -> AnyObject? in
            if let data = task.result as? NSData {
                let json = JSON(data: data)
                if let model = UploadModel.populateFromJSON(json) {
                    return BFTask(result: model)
                }
            }
            if let err = task.error {
                return BFTask(error: err)
            }
            return BFTask(error: ApiHelper.assembleError(ApiErrorCode.ParseResponseJSONError, errorMessage: nil))
        }).continueWithSuccessBlock({ [unowned self](task) -> AnyObject? in
            let model = task.result as! UploadModel
            var params:[String:AnyObject] = ["file": image]
            params.merge(model.toDictionary()!)
            print(params["key"])
            
            return self.baseHTTPFormRequest(model.method!, url: model.action!, params: params, requestHeaders: nil, keySequence:
                [
                "acl",
                "key",
                "X-Amz-Credential",
                "X-Amz-Algorithm",
                "X-Amz-Date",
                "Policy",
                "X-Amz-Signature",
                "file"
                ])
        })
    }
    
    // MARK: private function
    private static func prepareApiUrlForPath(path:String)-> String{
        return ApiHelper.SERVER_URL + path
    }
    
    // produce custom error without referencing "self"
    private static func assembleError(code:ApiErrorCode, errorMessage:String?)->NSError {
        return NSError(domain: ApiHelper.ERROR_DOMAIN, code: code.rawValue, userInfo: ["_ApiErrorMessage": errorMessage ?? code.defaultErrorMessage()])
    }
    
    private static func assembleError(code:ApiErrorCode, error:NSError)-> NSError {
        var userInfoWrapper:[NSObject: AnyObject]
        userInfoWrapper = ["_NSErrorCode" as NSString : NSNumber(integer: error.code), "_NSErrorDomain" as NSString: error.domain]
        userInfoWrapper.merge(error.userInfo)
        
        userInfoWrapper["_ApiErrorMessage"] = code.defaultErrorMessage()
        if error.domain == NSURLErrorDomain {
            switch error.code {
            case NSURLErrorTimedOut:
                userInfoWrapper["_ApiErrorMessage"] = "请求超时"
            case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost:
                userInfoWrapper["_ApiErrorMessage"] = "请求失败,请检测网络连接"
            case NSURLErrorCannotFindHost,NSURLErrorDNSLookupFailed, NSURLErrorCannotConnectToHost:
                userInfoWrapper["_ApiErrorMessage"] = "连接服务器失败"
            case NSURLErrorZeroByteResource:
                userInfoWrapper["_ApiErrorMessage"] = "服务器返回为空"
            default: break
            }
        }
        return NSError(domain: ApiHelper.ERROR_DOMAIN, code: code.rawValue, userInfo: userInfoWrapper)
    }
    
    private static func generateBoundaryString() -> String {
        return "----\(NSUUID().UUIDString)"
    }
    
    
    private static func handleResponse(bfs:BFTaskCompletionSource, data:NSData?, response:NSURLResponse?, error:NSError?, responseExpectedHeaders:[String]?) {
        if let err = error {
            return bfs.setError(ApiHelper.assembleError(ApiErrorCode.Internal, error: err))
        }
        if let retData = data {
            let resStr = NSString(data: retData, encoding: NSUTF8StringEncoding)
            print(resStr)
            if let res = response as? NSHTTPURLResponse {
                print(res.description)
                if res.allHeaderFields["Server"] as? String == "AmazonS3" {
                    if res.statusCode == 204 || res.statusCode == 200 || res.statusCode == 201 {
                        return bfs.setResult(retData)
                    } else {
                        return bfs.setError(ApiHelper.assembleError(ApiErrorCode.RequestDataInvalid, errorMessage: nil))
                    }
                }
            }
            
            var json = JSON(data: retData)
            if  json.error != nil {
                return bfs.setError(ApiHelper.assembleError(.ParseResponseJSONError, errorMessage: nil))
            } else {
                if let res = (response as? NSHTTPURLResponse) {
                    var headerError = false
                    var headerDict:[String: AnyObject]?
                    if let expected = responseExpectedHeaders {
                        for headerField in expected {
                            if res.allHeaderFields[headerField] == nil {
                                headerError = true
                                headerDict = nil
                                break
                            } else {
                                if headerDict == nil {
                                    headerDict = [:]
                                } else {
                                    headerDict![headerField] = res.allHeaderFields[headerField]
                                }
                            }
                        }
                    }
                    if headerError {
                        return bfs.setError(ApiHelper.assembleError(.ExpectedResponseHeadersMissing, errorMessage: nil))
                    } else {
                        if json.isExists() {
                            json["_statusCode"].number = NSNumber(integer: res.statusCode)
                            if headerDict != nil {
                                json["_headers"] = JSON(headerDict!)
                            }
                        }
                        
                        if res.statusCode != 200 && res.statusCode != 201 && res.statusCode != 204 {
                            return bfs.setError(ApiHelper.assembleError(ApiErrorCode.RequestDataInvalid, errorMessage: json["message"].string))
                        }
                        
                        do {
                            let result = try json.rawData()
                            return bfs.setResult(result)
                        } catch {
                            return bfs.setError(ApiHelper.assembleError(.ParseResponseJSONError, errorMessage: nil))
                        }
                    }
                    
                } else {
                    return bfs.setError(ApiHelper.assembleError(.ProtocolError, errorMessage: nil))
                }
            }
        } else {
            return bfs.setError(ApiHelper.assembleError(.EmptyResponse, errorMessage: nil))
        }
    }
    
    func baseHTTPFormPutRequest(url: String, params: [String: AnyObject],requestHeaders: [String: String]?)-> BFTask {
        return baseHTTPFormRequest("PUT", url: url, params: params, requestHeaders: requestHeaders)
    }
    
    func baseHTTPFormPostRequest(url: String, params: [String: AnyObject],
                                 requestHeaders:[String:String]?)->BFTask {
        return baseHTTPFormRequest("POST", url: url, params: params, requestHeaders: requestHeaders)
    }
    
    func baseHTTPFormRequest(method:String, url: String, params: [String: AnyObject],requestHeaders: [String: String]?, keySequence:[String]? = nil)-> BFTask {
        
        return BFTask(fromExecutor: BFExecutor(dispatchQueue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)), withBlock: { () -> AnyObject in
            let bfs:BFTaskCompletionSource = BFTaskCompletionSource()
            
            let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
            config.timeoutIntervalForRequest = ApiHelper.REQUEST_TIMEOUT
            
            let request = NSMutableURLRequest()
            request.URL = NSURL(string: url)!
            request.HTTPMethod = method
            
            request.setValue(ApiHelper.USER_AGENT, forHTTPHeaderField: "User-Agent")
            if requestHeaders != nil {
                for (headerField, headerVal) in requestHeaders! {
                    request.setValue(headerVal, forHTTPHeaderField: headerField)
                }
            }
            
            let boundary = ApiHelper.generateBoundaryString()
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            let body = NSMutableData()
            
            if let ks = keySequence {
                for key  in ks {
                    if let value = params[key] {
                        autoreleasepool {
                            if let image = value as? UIImage {
                                var finalImage = image
                                if image.size.width > 1028 {
                                    finalImage = image.resizeToWidth(1028)
                                }
                                let imageData = UIImageJPEGRepresentation(finalImage, 0.6)
                                
                                body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"file\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("Content-Type:image/jpeg \r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData(imageData!)
                                body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                            } else if (value as? NSString) != nil {
                                body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("Content-Disposition:form-data; name=\"\(key)\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("\(value)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                
                            } else if (value as? NSData) != nil {
                                // assume that the data represents an image
                                body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"file\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData("Content-Type:image/jpeg \r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                                body.appendData(value as! NSData)
                                body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                            }
                        }
                    }
                }
            }
            
            for (key, value) in params {
                if let ks = keySequence {
                    var shouldIgnoreKey = false
                    for kskey in ks {
                        if kskey == key {
                            shouldIgnoreKey = true
                            break
                        }
                    }
                    if shouldIgnoreKey {
                        continue
                    }
                }
                autoreleasepool {
                    if let image = value as? UIImage {
                        var finalImage = image
                        if image.size.width > 1028 {
                            finalImage = image.resizeToWidth(1028)
                        }
                        var imageData = UIImageJPEGRepresentation(finalImage, 1)
                        
                        if imageData!.length > 500000 {
                            imageData = UIImageJPEGRepresentation(finalImage, 0.6)
                        }
                        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"file\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("Content-Type:image/jpeg \r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData(imageData!)
                        body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                    } else if (value as? NSString) != nil {
                        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("Content-Disposition:form-data; name=\"\(key)\"\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("\(value)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        
                    } else if (value as? NSData) != nil {
                        // assume that the data represents an image
                        body.appendData("--\(boundary)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("Content-Disposition: form-data; name=\"\(key)\"; filename=\"file\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData("Content-Type:image/jpeg \r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                        body.appendData(value as! NSData)
                        body.appendData("\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                    }
                }
            }
            body.appendData("--\(boundary)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            
            request.HTTPBody = body
            
            let session = NSURLSession(configuration: config)
            
            let task = session.dataTaskWithRequest(request) {
                (let data, let response, let error) in
                ApiHelper.handleResponse(bfs, data: data, response: response, error: error, responseExpectedHeaders: nil)
            }
            task.resume()
            return bfs.task
        })
    }
    
    
    func baseHttpRequest(url:String, method:String, params: [String: AnyObject]?, requestHeaders: [String: String]?, responseExpectedHeaders:[String]?)->BFTask {
        
        
        //        let bfs:BFTaskCompletionSource = BFTaskCompletionSource()
        return BFTask(fromExecutor: BFExecutor(dispatchQueue: dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)), withBlock: { () -> AnyObject in
            
            let bfs = BFTaskCompletionSource()
            
            // don't save anything to session, making it stateless
            let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
            config.timeoutIntervalForRequest = ApiHelper.REQUEST_TIMEOUT
            
            let request = NSMutableURLRequest()
            request.URL = NSURL(string: url)!
            if method == "POST" || method == "PUT" {
                request.setValue("application/json", forHTTPHeaderField:"Content-Type")
            } else if method == "GET" {
                if params != nil {
                    let parameterString = params!.stringFromHttpParameters()
                    let requestURL = NSURL(string:"\(url)?\(parameterString)")
                    request.URL = requestURL
                } else {
                    request.URL = NSURL(string: url)!
                }
            } else if method != "DELETE"{
                bfs.setError(ApiHelper.assembleError(ApiErrorCode.ProtocolError, errorMessage: nil))
                return bfs.task
            }
            
            request.HTTPMethod = method
            request.setValue(ApiHelper.USER_AGENT, forHTTPHeaderField: "User-Agent")
            if requestHeaders != nil {
                for (headerField, headerVal) in requestHeaders! {
                    request.setValue(headerVal, forHTTPHeaderField: headerField)
                }
            }
            
            if params != nil && request.HTTPMethod != "GET" {
                do  {
                    request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params!, options: [])
                } catch {
                    bfs.setError(ApiHelper.assembleError(.ParseRequestJSONError, errorMessage: nil))
                    return bfs.task
                }
            }
            let session = NSURLSession(configuration: config)
            let task = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
                ApiHelper.handleResponse(bfs, data: data, response: response, error: error, responseExpectedHeaders: responseExpectedHeaders)
            })
            task.resume()
            return bfs.task
        })
    }
    
    func baseHTTPGetRequest(url:String, params: [String: AnyObject]?, requestHeaders: [String: String]?, responseExpectedHeaders:[String]?)->BFTask {
        return baseHttpRequest(url, method: "GET", params: params, requestHeaders: requestHeaders, responseExpectedHeaders: responseExpectedHeaders)
    }
    
    func baseHTTPGetRequest(url:String, params: [String: AnyObject]?, requestHeaders: [String: String]?)->BFTask {
        return baseHttpRequest(url, method: "GET", params: params, requestHeaders: requestHeaders, responseExpectedHeaders: nil)
    }
    
    
    func baseHTTPPostRequest(url:String, params: [String: AnyObject]?, requestHeaders: [String: String], responseExpectedHeaders:[String]?)->BFTask {
        return baseHttpRequest(url, method: "POST", params: params, requestHeaders: requestHeaders, responseExpectedHeaders: responseExpectedHeaders)
    }
    
    func baseHTTPPostRequest(url:String, params: [String: AnyObject]?, requestHeaders: [String: String])->BFTask {
        return baseHttpRequest(url, method: "POST", params: params, requestHeaders: requestHeaders, responseExpectedHeaders: nil)
    }
    
    func baseHTTPPostRequest(url:String, params: [String: AnyObject]?, responseExpectedHeaders:[String]?)->BFTask {
        return baseHttpRequest(url, method: "POST", params: params, requestHeaders: nil, responseExpectedHeaders: responseExpectedHeaders)
    }
    
    func baseHTTPPostRequest(url:String, params: [String: AnyObject]?)->BFTask {
        return baseHttpRequest(url, method: "POST", params: params, requestHeaders: nil, responseExpectedHeaders: nil)
    }
    
    // MARK: Singleton
    static let sharedInstance = ApiHelper()
    private init() {
        
    }
}